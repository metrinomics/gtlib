"""gtlib diverse metrinomics tools."""

# from distutils.core import setup
from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(
    name='gtlib',
    version='5.1',
    author="ruwen poljak",
    author_email="r.poljak@metrinomics.com",
    description="General python tools for metrinomics gmbh projects.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/metrinomics/gtlib",
    packages=['gtlib', 'gtlib.gbigram', 'gtlib.gobserver',
              'gtlib.gapi', 'gtlib.gql', 'gtlib.examples'],
    install_requires=['requests', 'python-dateutil']
)
