# GTLIB

### Bibliothek für die Projekte MC (Model Classifier), MG (Model Generator ) und Workflows.

bei Anpassungen von **gtlib** muss lokal folgendes ausgeführt werden:

- **pipenv update gtlib**
- das muss im jeweiligen Projekt-Verzeichnis, des jeweiligen Entwickler-PCs stattfinden.
- dabei wird ein neuer **Pipfile.lock** erzeugt.
- Pipfile.lock muss dann **commitet** werden!

# Versionen

## 5

### 5.1

- neues modul: gtlib/gtrivial
- erster docker wird testweise auf python 3.8 gesetzt, gtlib muss für beide versionen ( 3.6 und 3.8) funktionieren !
- behoben: /gtlib/gql/gql.py:28: SyntaxWarning: "is" with a literal. Did you mean "=="?
  if resItm is ""

## 4

### 4.7

- ignore .vscode

### 4.6

- gbigram.stm_word_dict - \_\_chr_to_del[] erweitert

### 4.5

- gbigram.stm_word_dict - \_\_chr_to_del[] erweitert

### 4.4

- gql.responses_by_respondent - hole auch respondentId als UUID ab

### 4.3

- gql.responses_by_respondent.my_sorter() - try/except

### 4.2

- gql.responses_by_respondent.my_sorter() - das ausgelieferte array ist nicht sortiert! hier wird nach answeredAt sortiert

### 4.1

- gconfig: TF_RESPONDENT_BASE_DATA_GRAPHQL_API - envvar gelöscht
- gconfig: TF_PM_REST_API - envvar gelöscht
- gql.gql.check_result() ergänzt, so das auch das ganze ergebnis von der wurzel an, zurück gegeben werden kann.

## 3

### 3.8

- gql.responses_by_respondent erstellt
- gql.base_data_respondent gelöscht
- gql.gql.check_result() ergänzt

### 3.7

- gql.answers() abholen von workflow-ergebnissen eingebaut
- gql.base_data_respondent() url-switch robuster umgesetzt

### 3.6

- neue Umgebungsvariablen definiert:
  - TF_RESPONDENT_BASE_DATA_GRAPHQL_API
  - TF_PM_REST_API
- gql.base_data_respondent : Abfrage Respondent Stammdaten
- gql.gql.\_\_do_login() : nicht jede API hat einen login !!!
- gapi.gapi.check_get_data_as_get() explizite get-version!
- gql.gql_respondent_base_data() pregnant und sex in abfrage aufgenommen.

### 3.5

- gapi.gapi.check_get_data() input variablen explizit benamt und default gesetzt
- gapi.gapi.pretty_uri() regex abfrage ersetzt durch string-prüfung (länge und inhalt)
- gapi.project() -> check_get_data() variablen explizit gesetzt

### 3.4

- gtlib.gtimeframe.calc*tf() umgebaut, in Teilfunktionen zerlegt ( calc_tf*\* ),
  damit ein externer Zugriff auf Teilfunktionen möglich ist!

- gtlib.gtimeframe.time*seg*\*() unterteilt daten in definierte zeitsegmente.

### 3.3

- gtlib.grules ausgelagert nach MC.rules.lib

### 3.2.13

- gapi/project und gql.project umbauten, reduktionen

### 3.2.12

- diverse funktionsnamen in gql.project.\_\_project_workflow_surveys() gekürzt
- project-abfrage via graphlql umgestellt

### 3.2.11

- gql.project.\_\_project_workflow_surveys() questions werden mit ausgegeben

### 3.2.10

- gql.project.gql_project()
  abfrage survey.questions modifiziert

  gql.project.\_\_project_workflow_surveys() ausgebaut, um abgefragte ergebnisse
  imn ergebnis-ausgabe zu integrieren

### 3.2.9

- gql.project.gql_project()
  abfrage ergänzt um surveys und survey.questions

### 3.2.8

- GTimeFrame
  gtimeframe.calc_tf() typo

### 3.2.7

- GTimeFrame
  time_difference_from_now_in_seconds() umgebaut

### 3.2.6

- GTimeFrame
  get_new_datetime_now()

### 3.2.5

- GTimeFrame
  set_end()
  ruft time_difference_from_now_in_seconds() auf

### 3.2.4

- GTimeFrame
  convert_datetime_to_unixtimestamp()
  Convert a datetime to unix timestamp.

### 3.2.3

- GTimeFrame
  set_now_from_unixtimestamp()
  setze timeobject via unix timestamp, utc vorausgesetzt

  get_as_unixtimestamp()
  set_now()

### 3.2.2

- GTimeFrame
  update_now() erzwingt ein update der initialen zeit!

### 3.2.1

- GTimeFrame
  - time_difference_from_now_in_seconds(), berechnet time-delta
  - \_\_calc_tf() zu calc_tf() umgebaut!

### 3.2

- GTimeFrame
  - time management tool
  - requieres dateutil lib ( python-dateutil )

### 3.1

- gapi.gapi.check_get_data()
  - FLASK entfernt, requests handling umgestellt

### 3.0

- Definitiver cut
- arbeiten am Corpus setzen hier offiziel an
- Alle Apps werden im Branch **corpus** entwickelt.
- Die Apps sind: MC (model_classifier), MG (model_generator), WF (workflows)

## 2

### 2.0.14

- long_description hinzugefügt
