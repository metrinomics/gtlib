"""library to prozess defined rules on given data.

Anwendung definierter Regeln ( json ), auf gegebene daten.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
"""

from gtlib.grules.atoms import RuleAtomsO
import logging


class MapRulesO:
    """Prozess rules on data."""

    def __init__(self):
        """Definiere Modelkonfiguration."""
        self.dataLine = {}
        self.__atom = RuleAtomsO()

    def check_rule(self, aStm, rule):
        """Wende Regel auf Datensatz an."""
        trueRuleItems = 0
        myRes = False
        for rkey in rule["input"].keys():
            try:
                kyLen = len(rule["input"][rkey])
                cruls = self.__rule_item(aStm, rule["input"], rkey)
                if cruls == kyLen:
                    trueRuleItems += 1
            except:
                pass

        if len(rule["input"]) == trueRuleItems:
            for ok in rule["output"].keys():
                self.dataLine[ok] = rule["output"][ok]
            myRes = True

        return myRes

    def __rule_item(self, aStm, rule, rkey):
        stmi = aStm[rkey]
        isFloat = False

        try:
            stmi = float(aStm[rkey])
            isFloat = True
        except:
            pass

        if isFloat is True:
            return self.__item_as_float(stmi, rule, rkey)
        else:
            # die werte werden als strings verglichen
            return self.__item_as_string(stmi, rule, rkey)

    def __item_as_float(self, stmi, rule, rkey):
        ml = "gtlib.grules.rules.__item_as_float"
        tru = 0
        for roi in rule[rkey].keys():
            try:
                try:
                    ri = float(rule[rkey][roi])
                except:
                    # a_in und a_not_in arbeiten mit arrays ....
                    ri = rule[rkey][roi]

                if self.__atom.a_greater(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_greater_eq(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_lower(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_lower_eq(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_not_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_in(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_not_in(roi, stmi, ri) == 1:
                    tru += 1
                    continue

            except Exception as e:
                logging.error("{}: {}".format(ml, e))
                pass

        return tru

    def __item_as_string(self, stmi, rule, rkey):
        ml = "gtlib.grules.rules.__item_as_string"
        tru = 0
        for roi in rule[rkey].keys():
            try:
                ri = rule[rkey][roi]
                if self.__atom.a_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_not_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_in(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__atom.a_not_in(roi, stmi, ri) == 1:
                    tru += 1
                    continue
            except Exception as e:
                logging.error("{}: {}".format(ml, e))
                pass

        return tru
