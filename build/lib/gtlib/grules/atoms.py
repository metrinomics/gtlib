"""Rule atoms.

Regel fragmente.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
"""

class RuleAtomsO:
    """Regel fragmente."""

    def __init__(self):
        """Definiere Modelkonfiguration."""

    def a_greater(self, roi, stmi, ri):
        try:
            if roi == "greater" and stmi > ri:
                return 1
        except:
            return 0
        return 0

    def a_greater_eq(self, roi, stmi, ri):
        try:
            if roi == "greater_equal":
                if stmi > ri or stmi == ri:
                    return 1
        except:
            return 0
        return 0

    def a_lower(self, roi, stmi, ri):
        try:
            if roi == "lower" and stmi < ri:
                return 1
        except:
            return 0
        return 0

    def a_lower_eq(self, roi, stmi, ri):
        try:
            if roi == "lower_equal":
                if stmi < ri or stmi == ri:
                    return 1
        except:
            return 0
        return 0

    def a_equal(self, roi, stmi, ri):
        try:
            if roi == "equal" and stmi == ri:
                return 1
        except:
            return 0
        return 0

    def a_not_equal(self, roi, stmi, ri):
        try:
            if roi == "not_equal" and stmi != ri:
                return 1
        except:
            return 0
        return 0

    def a_in(self, roi, stmi, ri):
        try:
            if roi == "in" and stmi in ri:
                return 1
        except:
            return 0
        return 0

    def a_not_in(self, roi, stmi, ri):
        try:
            if roi == "not_in" and stmi not in ri:
                return 1
        except:
            return 0
        return 0
