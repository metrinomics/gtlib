"""General APITools.

Es handelt sich um eine GraphQl abfrage, welche hier lediglich umgemappt
wird. Eine REST-Abfrage findet nicht statt!
"""

import requests
import logging
from gtlib.gql.project import ProjectO

class GApi:
    """General-API handling Tool."""

    def __init__(self,
        header={"Content-Type": "application/json"}, 
        protocol="https",
        host= "127.0.0.1",
        apitailwind= "", 
        email="", 
        passwd="", 
        url=""):
        """General-API handling Tool."""
        self.__conf = {
            "header" : header,
            "protocol" : protocol,
            "host" : host,
            "apitailwind" : apitailwind
        }

        self.__email = email
        self.__passwd = passwd
        self.__url = url

        self.__gql = ProjectO(email=self.__email, 
            passwd=self.__passwd, 
            url=self.__url)

    def __project_config_algo_is_dict(self, algo_out, iner, k1, k2):
        try:
            algo_out[k1][k2] = iner
        except:
            try:
                algo_out[k1][k2] = iner
            except:
                algo_out[k1] = {k2: iner}

    def __project_config_algo_no_dict(self, algo_out, iner, k1, k2):
        try:
            algo_out[k1][k1][k2] = iner
        except:
            iner2 = {k2: iner}
            try:
                algo_out[k1][k1] = iner2
            except:
                algo_out[k1] = {k1: iner2}

    def __project_config_algo(self, inData):
        # config im bereich config.algo umbauen
        algo_out = {}
        """
        es gibt folgende strukturen im config bereich eines projektes:
            bigram.bigram.bigram01.active
            fake.random_float.active
        gewollt ist die erste struktur. es wird auf active geprüft
        eine falsche struktur wird wie folgt umgebaut:
            fake.fake.random_float.active
        """
        try:
            for k1 in inData["config"]["algo"].keys():
                for k2 in inData["config"]["algo"][k1].keys():
                    iner = inData["config"]["algo"][k1][k2]
                    if inData["config"]["algo"][k1][k2].get("active"):
                        self.__project_config_algo_no_dict(
                            algo_out, iner, k1, k2)
                    else:
                        self.__project_config_algo_is_dict(
                            algo_out, iner, k1, k2)
        except Exception as e:
            logging.error("GApi.__project_config_algo: {}".format(e))
            logging.error(inData)

        try:
            del inData["config"]["algo"]
            inData["config"]["algo"] = algo_out
        except Exception as e:
            logging.error("GApi.__project_config_algo (2): {}".format(e))
            logging.error(inData)

        return inData

    def read_map(self, myMap):
        """Read a map."""
        a_uri = "{}/maps/{}".format(self.__conf['apitailwind'], int(myMap))
        rp = requests.get(a_uri)
        inData = self.check_status_code(rp)
        return inData["content"]

    def get_projects(self):
        """Read all project definitions."""
        """
        @TODO:
        analog zu get_project umbauen!
        """
        ml = "gtlib.gapi.get_projects"
        inData = []
        try:
            a_uri = "{}/projects".format(self.__conf['apitailwind'])
            rp = requests.get(a_uri)
            rpj = self.check_status_code(rp)
            for pr in rpj:
                inData.append(self.get_project(pr['id']))
        except Exception as e:
            logging.error("{} : {}".format(ml, e))

        return inData

    def get_project(self, myProject):
        """Read project definition."""
        ml = "gtlib.gapi.get_project"
        inData = {}
        try:
            pr = self.__gql.get_project(myProject)
            # self.__project_config_algo(pr)
            inData = self.__project_config_algo(pr)
        except Exception as e:
            logging.error("{} : {}".format(ml, e))

        return inData

    def check_get_data(self, a_uri, jsBody={}):
        """Get data and check it."""
        rp = requests.post(self.pretty_uri(a_uri), json=jsBody,
                           headers=self.__conf['header'])
        return self.check_status_code(rp)

    def check_status_code(self, rp):
        """Check status code of retrieved data."""
        inData = []
        if rp.status_code in [200, 201]:
            try:
                inData = rp.json()
            except Exception as e:
                logging.debug("GApi.check_status_code: {}".format(e))
        return inData

    def pretty_uri(self, a_uri=""):
        """Build a pretty uri."""
        myUri = "{}://{}".format(
            self.__conf['protocol'],
            self.__conf['host'])

        if a_uri != "":
            myUri = "{}/{}".format(myUri, a_uri)

        return myUri
