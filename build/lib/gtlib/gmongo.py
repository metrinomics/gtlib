"""Gmongo möglichst allgemeiner API-Zugriff auf MongoDB."""

import os
import pymongo
import logging


class MongoAPI:
    """Mongo API Object."""

    def __init__(self):
        """Mongo API."""
        self.driver = ""
        self.db = ""
        self.__aFilter = ""
        self.__do_driver()

    def __do_driver(self):
        my_uri = "mongodb://{}:{}@{}/{}?authSource={}".format(
            os.environ['MONGO_USER'],
            os.environ['MONGO_PASSWORD'],
            "{}:{}".format(os.environ['MONGO_HOST'], os.environ['MONGO_PORT']),
            os.environ['MONGO_PASSWORD'],
            self.__config['MONGO_DB'],
            self.__config['MONGO_DB'],
            self.__config['MONGO_DB_AUTH']
        )
        self.driver = pymongo.MongoClient(my_uri)
        self.db = self.driver['model_generator']
