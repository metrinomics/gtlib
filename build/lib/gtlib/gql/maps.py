"""GQL Client lib.

realisiert mit dem requests modul
@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
import requests
import logging

class MapsO(GqlO):
    """GQL Client lib."""

    def get_map(self, map_id=0):
        """Hole Map."""
        qu = {
            'query': '''query{{
                map(id:{}){{  
                    content
                    description
                    id
                    type
                    title }}
                }}'''.format(map_id)
        }

        myMap = self.check_result("map", qu)
        return myMap["content"]

    def get_maps(self):
        """Hole maps Übersicht."""
        qu = {
            'query': '''query{
                maps{  
                    title 
                    type 
                    id  
                    description }
                }'''
        }

        return self.check_result("maps", qu)
