"""GQL Client lib.

@copyright: 2020 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
# import logging


class AnswerO(GqlO):
    """GQL Client Answer."""

    def __init__(self, configO):
        GqlO.__init__(self, configO)

    def gql_answer(self, answer_id=0):
        qu = {
            'query': '''query{{
                answer(id:{}){{
                    id 
                    statements {{
                        id
                        content
                        response {{ 
                            id 
                            respondentId
                        }}
                    }}                    
                }}
            }}'''.format(answer_id)}
        nu = self.check_result("answer", qu)
        return nu
