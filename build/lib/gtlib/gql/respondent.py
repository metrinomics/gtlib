"""GQL Client lib.

@copyright: 2020 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
# import logging
# import json


class RespondentO(GqlO):
    """GQL Client lib."""

    def __init__(self, configO):
        GqlO.__init__(self, configO)

    def get_respondent(self, respondent_id=""):
        """Hole Map."""
        qu = {
            'query': '''query{{
                getResponse(id:'{}'){{  
                    respondentId
                    answers {{
                        id
                    }}
                }}'''.format(respondent_id)
        }

        myResp = self.check_result("respondent", qu)
        return myResp["content"]
