"""GQL Client lib.

@copyright: 2020 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
import copy
# import logging


class BaseDataRespondentO(GqlO):
    """GQL Client Base Data."""

    def __init__(self, configO):
        self.__configO = copy.copy(configO)
        self.__configO.url = self.__configO.tf_respondent_base_data_graphql_api
        GqlO.__init__(self, configO)

    def gql_respondent_base_data(self, respondent_id=""):
        qu = {
            'query': '''query{{
                getContact(uid:"{}"){{
                    firstName,
                    lastName,
                    diseases,
                    birthdate,
                    pregnant,
                    sex
                }}
            }}'''.format(respondent_id)}
        nu = self.check_result("getContact", qu)
        return nu
