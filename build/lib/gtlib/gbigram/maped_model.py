"""Read model and expand it with maps.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@note:
liest ein bigram model ein. stopwords werden aus dem model entfernt.
der key-begriff des models wird in die bigram liste aufgenommen. alle
bigramme werden um worte aus dem mapping-liste ergänzt. es wird eine
inside-out version (self.cls_mod_re) der bigramme erzeugt, um so einen
schnellen zugriff auf die bigramme zu ermöglichen.
"""

import logging


class MapedModelO:
    """Read model and expand it with maps."""

    def __init__(self, cls_mod, maps):
        """Read model and expand it with maps."""
        self.__mms = maps
        self.cls_mod = cls_mod
        self.cls_mod_re = {}

        self.__clear_and_map_model()
        self.__inside_out()

    def __inside_out(self):
        for mi in self.cls_mod:
            for bi in mi["bigrams"]:
                try:
                    self.cls_mod_re[bi["word"]][mi["key"]] = bi["sig"]
                except:
                    self.cls_mod_re[bi["word"]] = {mi["key"]: bi["sig"]}

    def __extend_bigrams_with_map(self, i):
        toAppend = []

        for bi in i["bigrams"]:
            theWrd = bi["word"]
            """schreibe den key, das gemappte wort!"""
            try:
                if self.__mms.wmap_re[bi["word"]]:
                    theWrd = self.__mms.wmap_re[bi["word"]]
                    aro = {"word": theWrd, "sig": bi["sig"]}
                    toAppend.append(aro)
            except:
                pass

            """schreibe mapping worte rein, nicht den key"""
            try:
                for wm in self.__mms.wmap[theWrd]:
                    aro = {"word": wm, "sig": bi["sig"]}
                    toAppend.append(aro)
            except:
                pass

        if len(toAppend) > 0:
            i["bigrams"] += toAppend

    def __clear_and_map_model_key(self, i):
        # key wort in bigram-liste aufnehmen
        if i["key"] not in self.__mms.wstop:
            # kein stopword, versuche map
            aro = {"word": i["key"], "sig": 1.0}
            try:
                if self.__mms.wmap_re[i["key"]]:
                    aro["word"] = self.__mms.wmap_re[i["key"]]
            except:
                pass
            i["bigrams"].append(aro)
        else:
            # das ist ein stopword!?
            logging.debug("stopword as part of a model: {}".format(
                i["key"]))

    def __clear_and_map_model(self):
        # erweitere model um maps
        for i in self.cls_mod:
            self.__clear_and_map_model_key(i)
            self.__extend_bigrams_with_map(i)
