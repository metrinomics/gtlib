"""Maps: maps, merge, stop.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@note:
Lese MapListe (self.wmap), MergeListe (self.wmerge).
Bilde von Map- und Mergelisten jeweils
eine InsideOut (self.wmap_re, self.wmerge_re) Liste, das dient dem schnellen
Zugriff auf die Elemente einer Klasse, welche in den initialen listen in
Arrays liegen und in den InsideOut Listen zu Keys auf oberster Ebene werden.
"""

import logging


class MapMergeStopO:
    """Word Maps."""

    def __init__(self):
        """Word Maps."""
        self.wmap = {}
        self.wmap_re = {}
        self.wmerge = {}
        self.wmerge_re = {}
        self.wstop = []

    def extend_wstop_with_wmap(self):
        """Extend Stopwords with maps."""
        more_stopwords = {}
        for sw in self.wstop:
            if sw in self.wmap_re.keys():
                if sw not in more_stopwords:
                    more_stopwords[sw] = True
                    for swr in self.wmap[self.wmap_re[sw]]:
                        more_stopwords[swr] = True
                    more_stopwords[self.wmap_re[sw]] = True

        if more_stopwords != {}:
            nw = list(more_stopwords.keys())
            self.wstop += nw

    def make_wstop(self, word_stop):
        """Word Maps Stopwords."""
        ml = "gbigram.map_merge_stop.make_wstop"
        try:
            self.wstop = word_stop["stopwords"]
        except Exception as e:
            logging.debug("{} stop: {}".format(ml, e))

    def make_wmap(self, word_map):
        """Word Maps Maping Words."""
        ml = "gbigram.map_merge_stop.make_wmap"
        try:
            self.wmap = word_map
            """
                das innere array wird als key nach aussen gelegt, mit dem
                eigentlichen key als value. das soll den zugriff beschleunigen
            """
            for ki in self.wmap.keys():
                for vi in self.wmap[ki]:
                    self.wmap_re[vi] = ki
        except Exception as e:
            logging.debug("{} map: {}".format(ml, e))

    def make_wmerge(self, word_merge):
        """Word Maps Merge Words."""
        ml = "gbigram.map_merge_stop.make_wmerge"
        try:
            self.wmerge = word_merge
            # siehe anmerkung self.word_map_re
            for ki in self.wmerge.keys():
                for vi in self.wmerge[ki]:
                    try:
                        self.wmerge_re[vi[0]][vi[1]] = ki
                    except:
                        self.wmerge_re[vi[0]] = {vi[1]: ki}

        except Exception as e:
            logging.debug("{} merge: {}".format(ml, e))
