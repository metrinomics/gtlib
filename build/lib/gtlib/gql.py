"""GQL Client lib.

realisiert mit dem requests modul
@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

import requests
import logging

class GqlO:
    """GQL Client lib."""

    def __init__(self, email="", passwd="", url=""):
        """Etabliere Verbindung, hole token."""
        self.__email = email
        self.__passwd = passwd
        self.__url = url
        self.__headers = {}

        self.__do_login()

    def __check_result(self, resItm="", qu=""):
        res = self.__post_data(qu)
        stm = ""
        try:
            if res.status_code == 200:
                rej = res.json()
                if resItm is "":
                    stm = rej["data"]["project"]
                else:
                    stm = rej["data"]["project"][resItm]
            else:
                logging.debug(res.status_code)
                logging.debug(res.text)
                logging.debug(qu)
        except Exception as e:
            logging.error("gtlib.gql.gql.__check_result()")
            logging.error(qu)
            logging.error(rej)
            logging.error(e)

        return stm

    def get_ratingsOverTime(self, project_id=0, project_workflow_id=0):
        """Hole ratingsOverTime von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    ratingsOverTime(projectWorkflowId:{}){{
                        date, value
                        }}
                    }}
                }}'''.format(project_id, project_workflow_id)
        }
        return self.__check_result("ratingsOverTime", qu)

    def get_expectation_analysis(self, project_id=0, project_workflow_id=0):
        """Hole expectationAnalysis von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    expectationAnalysis(projectWorkflowId:{}){{
                        low {{ betterCount, expectedCount, worseCount }},
                        average {{ betterCount, expectedCount, worseCount }},
                        high {{ betterCount, expectedCount, worseCount }}
                        }}
                    }}
                }}'''.format(project_id, project_workflow_id)
        }

        return self.__check_result("expectationAnalysis", qu)

    def gql_streaming_overview(self, project_id=0, project_workflow_id=0):
        """Hole streamingOverview von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    streamingOverview(projectWorkflowId:{}){{
                        averagePositivity,
                        totalCount,
                        triggersCount,
                        topics{{
                            numberOfTopicsAllRatings,
                            numberOfTopics,
                            count,
                            label,
                            positivity }}
                         }}}}}}'''.format(
                        project_id, project_workflow_id)
        }

        return self.__check_result("streamingOverview", qu)

    def get_health_score(self, project_id=0, project_workflow_id=0):
        """Hole halthScore von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    healthScore(project_workflow_id:{}){{
                        badRatingsCount,
                        mediumRatingsCount,
                        goodRatingsCount }}}}}}'''.format(
                        project_id, project_workflow_id)
        }

        return self.__check_result("healthScore", qu)

    def get_topic_positivity(self, project_id=0, project_workflow_id=0):
        """Hole Topic Positivity worte zu einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    topicPositivity(project_workflow_id:{}){{
                        label, negativeCount, positiveCount}}}}}}'''.format(
                        project_id, project_workflow_id)
        }

        return self.__check_result("topicPositivity", qu)

    def get_top_topics_no_synonyms(self, project_id=0, numberOfTopics=3,
                                   numberOfWords=3):
        """Hole Top X worte zu einem Projekt, exkludiere synonyme."""
        return self.get_top_topics(
            project_id, numberOfTopics, numberOfWords, "false")

    def get_top_topics(self, project_id=0, numberOfTopics=3,
                       numberOfWords=3, synonyms="true"):
        """Hole Top X worte zu einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    topTopics(numberOfTopics: {}, numberOfWords: {},
                        synonyms: {} ){{
                        mostFrequentWords, position, topic }}}}}}'''.format(
                        project_id, numberOfTopics, numberOfWords, synonyms)
        }

        return self.__check_result("topTopics", qu)

    def __project_config_workflow(self, qu):
        try:
            wfs = qu["projectEnabledWorkflows"][0]
            wft_l = wfs["workflow"]["type"]
            wft_s = wfs["active"]
            return {  wft_l : wft_s }
        except:
            return {}

    def __project_config_algo_item(self,suix, itm):
        if itm.get("active") in [None, False]:
            return True

        if itm["active"] is True:
            il = itm["label"]
            if suix.get(il) is None or len(suix[il]) == 0:
                suix[il] = {
                    "active" : itm["active"],
                    "configuration" : itm["configuration"]
                }

        return True

    def __project_workflow_steps_model(self, so):
        aModel = {
            "build" : "last",
            "label" : "",
            "type" : "" 
        }

        if so.get("model") in [None, False]:
            return aModel    
        
        st = so["model"]

        if st.get("label"):
            aModel["label"] = st["label"]

        if st.get("modelBuild"):
            aModel["build"] = st["modelBuild"]

        if st.get("algorithm") and st["algorithm"].get("suite"):
                aModel["type"] = st["algorithm"]["suite"]

        return aModel

    def __project_workflow_steps_map(self, so):
        aMap = []

        if so.get("map") in [None, False]:
            return aMap

        if so["map"].get("content") in [None, False]:
            return aMap

        st = so["map"]["content"]

        for ky in st.keys():
            aMap.append( { ky : st[ky] } )
        return aMap

    def __project_workflow_steps(self, su):
        au = []
        sts = sorted( su["steps"], key=lambda x: x["stepOrder"])
        for st in sts: 
            st1 = {
                "action" : st["action"],
                "map" : self.__project_workflow_steps_map(st),
                "model" : self.__project_workflow_steps_model(st)
            }
            au.append(st1)
        return au

    def __project_workflow_surveys(self, su):
        au = []
        for si in su["surveys"]:
            au.append( { "id" : si["id"] } )
        return au

    def __project_workflow_head(self, su):
        steps = self.__project_workflow_steps(su)
        surveys = self.__project_workflow_surveys(su)
        aWf = { 
            "active" : su["active"],
            "id" : su["id"],
            "label" : su["label"],
            "type" : su["type"],
            "desc" : su["description"],
            "steps" : steps,
            "surveys" : surveys
        }
        return aWf

    def __project_workflow(self, qu):
        try:
            wfl = []
            for su in qu["projectWorkflows"]:
                if su.get("active") in [ None, False ]: 
                    continue
                if su["project"]["id"] != qu["id"]:
                    continue
                wfl.append(self.__project_workflow_head(su))
            return wfl
        except:
            return []

    def __project_model(self, qu):
        try:
            mod_id = {}
            for su in qu["projectEnabledAlgorithms"]:
                if su.get("algorithm"):
                    if su["algorithm"].get("active") in [ None, False ]:
                        continue
                    if su["algorithm"].get("models") is None:
                        continue
                    if len(su["algorithm"]["models"]) == 0:
                        continue

                    self.__project_model_itm(qu["id"], mod_id, su["algorithm"])

            mod = []
            for ky in mod_id.keys():
                mod.append(mod_id[ky])

            return mod
        except:
            return []

    def __project_model_itm(self, project_id, mod_id, algo):
        for model in algo["models"]:
            if project_id != model["project"]["id"]:
                continue
            if model.get("active") in [None, False]:
                continue

            if mod_id.get(model["id"]) is None:
                mod_id[model["id"]] = {
                    "active": model["active"],
                    "id": model["id"],
                    "label": model["label"],
                    "desc": model["description"],
                    "type": model["algorithm"]["suite"],
                    "algo": model["algorithm"]["label"],
                    "build_size": model["buildSize"],
                    "configuration": model["configuration"],
                    "update": {
                        "data_pool": model["updateDataPool"],
                        "size_trigger": model["updateSizeTrigger"],
                        "time_trigger": model["timeTrigger"] 
                        } }

    def __project_config_algo_head(self, qu, sui, algo):
        project_id = qu["id"]
        l_suit = algo["suite"] 
        l_algo = algo["label"]

        if sui.get( l_suit ) is None:
            sui[l_suit] = {}

        if sui[l_suit].get( l_algo ) is None:
            sui[l_suit][l_algo] = {}



        if algo.get("models") is None or len(algo["models"]) == 0:
            self.__project_config_algo_item(
                sui[l_suit], algo)
        else:
            for model in algo["models"]:                
                if project_id == model["project"]["id"]:
                    self.__project_config_algo_item(
                        sui[l_suit][l_algo], model)

        return True

    def __project_config_algo(self, qu):
        try:
            sui = {}
            for su in qu["projectEnabledAlgorithms"]:
                if su.get("algorithm"):
                    if su["algorithm"].get("active") in [ None, False ]:
                        continue
                    self.__project_config_algo_head(qu, sui, su["algorithm"])
            return sui
        except:
            return {}

    def __project_map(self, qu):
        algo_out = {
            "id": qu["id"],
            "label": qu["name"],
            "company": qu["company"]["id"],
            "config": {
                "algo": {},
                "workflow": {}
                },
            "workflow": [],
            "model" : []
        }

        algo_out["config"]["workflow"] = self.__project_config_workflow(qu)
        algo_out["config"]["algo"] = self.__project_config_algo(qu)
        algo_out["model"] = self.__project_model(qu)
        algo_out["workflow"] = self.__project_workflow(qu)
        return algo_out

    def get_project(self, project_id=0):
        qu = {
            'query': '''query{{
                project(id:{}){{
                    name
                    id
                    company {{id}}
                    projectWorkflows {{
                      active
                      id
                      label
                      type
                      description
                      steps {{  
                        action
                        map {{ content }}
                        model {{
                          id
                          label
                          algorithm {{ label, suite }}
                        }}
                        stepOrder
                      }}
                      project {{ id }}
                      surveys {{ id }}
                    }}
                    projectEnabledWorkflows {{
                        active
                        workflow {{ type }}
                    }}
                    projectEnabledAlgorithms {{
                        algorithm {{
                            active
                            configuration
                            active
                            description
                            suite
                            label
                            models {{
                                label
                                id
                                active
                                configuration
                                description
                                buildSize
                                updateDataPool
                                updateSizeTrigger
                                timeTrigger
                                project {{ id }}
                                algorithm {{
                                    label
                                    suite
                                }}
                            }}                            
                        }}
                    }}
                }}
            }}'''.format( project_id) }

        nu = self.__check_result("", qu)
        return self.__project_map(nu)

    def get_statements(self, project_id=0):
        """Hole alle Statements zu einem projekt."""
        """
        {
          answerFeed(projectId:1, limit:20) {
            answers { id, statements{ id, content } }
            total } }
        """

        qu = {
            'query': '''query{{
                project(id:{}){{
                    answers{{
                        id,
                        answeredAt,
                        rating {{ id, label, position }}
                        statements {{id, content}} }} }} }}'''.format(
                        project_id)
        }

        return self.__check_result("answers", qu)

    def __do_login(self):
        md = "GqlO.__do_login"
        qu = {
            'query': '''mutation{{
                login(email: "{}", password:"{}"){{
                    token}}}}'''.format(
                self.__email, self.__passwd)
        }
        res = requests.post(self.__url, json=qu)

        if res.status_code == 200:
            rej = res.json()
            token = rej["data"]["login"]["token"]
            self.__headers['Authorization'] = 'Bearer {}'.format(token)
        else:
            print("login failed!")
            logging.error("{} : {}".format(md, res))

    def __post_data(self, my_json):
        res = requests.post(
            self.__url,
            json=my_json,
            headers=self.__headers
        )
        return res
