"""General APITools Project.

Es handelt sich um eine GraphQl abfrage, welche hier lediglich umgemappt
wird. Eine REST-Abfrage findet nicht statt!
"""

from gtlib.gql.project import ProjectO
from gtlib.gapi.gapi import GApi
import logging


class ProjectApi:
    """General-API handling Tool."""

    def __init__(self, configO):
        self.__configO = configO
        self.__mll = "gtlib.gapi.project."

    def get_project(self, all_args):
        """Read project definition.
            hier gibt es eine graphql version
            und eine rest-variante
            aufruf grapgql: /project/1
            aufruf rest:  /project/1/db_context
        """
        project_id = 0
        try:
            if all_args.get("project"):
                project_id = "{}".format(all_args["project"])
        except:
            pass

        real_rest_api = False
        try:
            if all_args.get("rest_api"):
                real_rest_api = all_args["rest_api"]
        except:
            pass

        inData = {}
        if real_rest_api is False:
            inData = self.__rest_fake(project_id)
        else:
            inData = self.__rest_real(project_id)

        return inData

    def __rest_real(self, project_id):
        ml = "{}__rest_real".format(self.__mll)
        inData = {}
        prO = GApi(self.__configO)
        try:
            inData = prO.check_get_data(
                a_uri=project_id, jsBody={}, full_url=self.__configO.tf_project_rest_api)
        except Exception as e:
            logging.error("{} : {}".format(ml, e))
        return inData

    def __rest_fake(self, project_id):
        ml = "{}__rest_fake".format(self.__mll)
        inData = {}
        prO = ProjectO(self.__configO)
        try:
            pr = prO.gql_project(project_id)
            inData = self.__cfga(pr)
        except Exception as e:
            logging.error("{} : {}".format(ml, e))
        return inData

    def get_projects(self):
        """Read all project definitions."""
        ml = "{}get_projects".format(self.__mll)
        inData = []
        prO = ProjectO(self.__configO)
        try:
            rpj = prO.gql_projects()
            for pr in rpj:
                inData.append(self.get_project({
                    'project': "{}".format(pr['id']),
                    'rest_api': False}))
        except Exception as e:
            logging.error("{} : {}".format(ml, e))

        return inData

    def __cfga_is_dict(self, algo_out, iner, k1, k2):
        try:
            algo_out[k1][k2] = iner
        except:
            try:
                algo_out[k1][k2] = iner
            except:
                algo_out[k1] = {k2: iner}

    def __cfga_no_dict(self, algo_out, iner, k1, k2):
        try:
            algo_out[k1][k1][k2] = iner
        except:
            iner2 = {k2: iner}
            try:
                algo_out[k1][k1] = iner2
            except:
                algo_out[k1] = {k1: iner2}

    def __cfga(self, inData):
        # config im bereich config.algo umbauen
        ml = "{}__cfga".format(self.__mll)
        algo_out = {}
        """
        es gibt folgende strukturen im config bereich eines projektes:
            bigram.bigram.bigram01.active
            fake.random_float.active
        gewollt ist die erste struktur. es wird auf active geprüft
        eine falsche struktur wird wie folgt umgebaut:
            fake.fake.random_float.active
        """
        try:
            for k1 in inData["config"]["algo"].keys():
                for k2 in inData["config"]["algo"][k1].keys():
                    iner = inData["config"]["algo"][k1][k2]
                    if inData["config"]["algo"][k1][k2].get("active"):
                        self.__cfga_no_dict(
                            algo_out, iner, k1, k2)
                    else:
                        self.__cfga_is_dict(
                            algo_out, iner, k1, k2)
        except Exception as e:
            logging.error("{}: {}".format(ml, e))
            logging.error(inData)

        try:
            del inData["config"]["algo"]
            inData["config"]["algo"] = algo_out
        except Exception as e:
            logging.error("{} (2): {}".format(ml, e))
            logging.error(inData)

        return inData
