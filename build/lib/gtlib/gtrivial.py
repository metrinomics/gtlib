"""General Tools."""

import logging


class GTrivial:
    """Triviales."""

    def __init__(self):
        """Triviales."""
        pass

    def print_version(self, myMod=[]):
        """Print Version."""

        try:
            print("\n{}".format("-"*60))
            print("""Version {}: {}\npython: {}""".format(
                myMod['label'], myMod['version'], myMod['python']))
            print("{}\n".format("-"*60))
        except Exception as e:
            ml = "gtlib.GTrivial.get_model_def"
            logging.debug("{}: {}".format(ml, e))
        return True
