"""ORM Maper Objects.

@copyright: 2019 Metrinomics GmbH. All rights reserved.

suhas.org/sqlalchemy-tutorial/

Das folgende Python-Script hilft beim anlegen der DB,
allerdings benötigt es zugriff auf gtlib

'''Test SqlAlchemy.'''

import sys
import os
sys.path.append("../")
from gtlib.gcorpus.model import CorpusModel
from gtlib.gpgsql import GPgSql

os.environ['POSTGRESSERVER'] = "127.0.0.1:5432"
os.environ['POSTGRESUSER'] = "tailwind"
os.environ['POSTGRESPWD'] = "tailwind"
os.environ['POSTGRESDB'] = "tailwind"
dbO = GPgSql()
con, meta = dbO.getConMeta()
crp = CorpusModel(con, meta)
crp.create_db()
print("---THE END---")
"""

# import logging
# from sqlalchemy import Table
# from sqlalchemy import Index
# from sqlalchemy.orm import composite
from sqlalchemy import Column
from sqlalchemy import ForeignKey, ForeignKeyConstraint
from sqlalchemy import Integer, Float, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
# from sqlalchemy_utils import CompositeType
# from sqlalchemy.orm import sessionmaker

Base = declarative_base()


class CorpusModel:
    """Defines corpus model, also for DB."""

    def __init__(self, con, meta):
        """Init with con and meta of DB."""
        self.__con = con
        self.__meta = meta

    def create_db(self):
        """Create the DB."""
        Base.metadata.create_all(self.__con)


class Project(Base):
    """Project Object."""

    __tablename__ = "project"

    id = Column(Integer, primary_key=True)
    project = Column(Integer, index=True, nullable=False)
    label = Column(String, index=True, nullable=False)


class Corpus(Base):
    """Corpus Object."""

    """
    type = [ "overall", "year", "week", "month", "day" ]
    status = [ "init", "calc", "ready" ]
    """

    __tablename__ = "corpus"

    id = Column(Integer, primary_key=True)
    project = Column(Integer, ForeignKey(Project.id), nullable=False)
    label = Column(String, index=True, nullable=False)
    type = Column(String, nullable=False, default="overall")
    date_start = Column(DateTime)
    date_end = Column(DateTime)
    status = Column(String, nullable=False, default="init")
    recalc_time = Column(Float, default=0.0)
    count_sentence = Column(Integer, default=0)
    count_word = Column(Integer, default=0)
    count_bigram = Column(Integer, default=0)
    max_freq_word = Column(Integer, default=0)
    max_freq_bigram = Column(Integer, default=0)
    max_sig = Column(Float, default=0.0)
    max_sigfreq = Column(Float, default=0.0)

    """
    ForeignKeyConstraint(
        ["project", "label", "type"],
        ["project", "label", "type"],
        name="project_label_type")
    """
    # order_by = ['-project', 'label', 'type']


class Word(Base):
    """Word Object."""

    __tablename__ = "word"
    id = Column(Integer, primary_key=True)
    project = Column(Integer, ForeignKey(Project.id), nullable=False)
    word = Column(String, index=True, nullable=False)

    # ForeignKeyConstraint(["project"], ["word"], name="project_word")
    # order_by = ['-project', 'word']


class Corpus_word(Base):
    """Corpus_word Object."""

    __tablename__ = "corpus_word"
    id = Column(Integer, primary_key=True)
    corpus = Column(Integer, ForeignKey(Corpus.id), nullable=False)
    word = Column(Integer, ForeignKey(Word.id), nullable=False)
    freq = Column(Integer, default=0)

    # ForeignKeyConstraint(["corpus"], ["word"], name="corpus_word")
    # order_by = ['-corpus', 'word', 'freq']


class Bigram(Base):
    """Bigram Object."""

    """
        ACHTUNG a und b muessen sortiert abgelegt werden um eine redundante
        speicherung von b und a, zu vermeiden!
        die direkte verlinkung auf den zugehörigen corpus ist nicht unbedingt
        notwendig, erhöht jedoch übersicht und nachvollziehbarkeit
    """

    __tablename__ = "bigram"
    id = Column(Integer, primary_key=True)
    corpus = Column(Integer, ForeignKey(Corpus.id), nullable=False)
    a = Column(Integer, ForeignKey(Corpus_word.id), nullable=False)
    b = Column(Integer, ForeignKey(Corpus_word.id), nullable=False)
    # satz frequenz zum zeitpunkt der bigram berechnung
    freq_s = Column(Integer, default=1)

    # wort frequenzen zum berechnungszeitpkt
    freq_a = Column(Integer, default=1)
    freq_b = Column(Integer, default=1)
    freq_ab = Column(Integer, default=1, index=True)
    sig = Column(Float, default=0.0, index=True)
    # freq_ab * sig
    sigfreq = Column(Float, default=0.0, index=True)

    # ForeignKeyConstraint(["corpus"], ["a"], ["b"], name="corpus_a_b")
    # order_by = ['-corpus', 'a', 'b']


class Sentence(Base):
    """Sentence Object."""

    __tablename__ = "sentence"
    id = Column(Integer, primary_key=True)
    project = Column(Integer, ForeignKey(Project.id), nullable=False)
    statement = Column(Integer, nullable=False)
    pos = Column(Integer, default=0)

    """
    ForeignKeyConstraint(["project"], ["statement"], ["pos"],
                         name="project_statement_pos")
    """
    # order_by = ['-project', '-statement', 'pos']


class Sentence_word(Base):
    """Sentence_word Relation-Object."""

    __tablename__ = "sentence_word"
    id = Column(Integer, primary_key=True)
    project = Column(Integer, ForeignKey(Project.id), nullable=False)
    sentence = Column(Integer, ForeignKey(Sentence.id), nullable=False)
    word = Column(Integer, ForeignKey(Word.id), nullable=False)
    pos = Column(Integer, default=0)

    """
    ForeignKeyConstraint(["project"], ["sentence"], ["word"],
                         name="project_sentence_word")
    """
    # order_by = ['-project', '-sentence', 'pos', 'word']
