"""library to use defined rules on given data.

Anwendung definierter Regeln ( json ), auf gegebene daten.

@copyright: 2018 Metrinomics GmbH. All rights reserved.
"""


class MapRulesO:
    """Map rating to another scala."""

    def __init__(self):
        """Definiere Modelkonfiguration."""
        self.dataLine = {}

    def check_rule(self, aStm, rule):
        """Wende Regel auf Datensatz an."""
        trueRuleItems = 0
        myRes = False
        for rkey in rule["input"].keys():
            try:
                kyLen = len(rule["input"][rkey])
                cruls = self.__crit(aStm, rule["input"], rkey)
                if cruls == kyLen:
                    trueRuleItems += 1
            except:
                pass

        if len(rule["input"]) == trueRuleItems:
            for ok in rule["output"].keys():
                self.dataLine[ok] = rule["output"][ok]
            myRes = True
            # return True
        # return False

        return myRes

    def __crit(self, aStm, rule, rkey):
        stmi = aStm[rkey]
        isFloat = False

        try:
            stmi = float(aStm[rkey])
            isFloat = True
        except:
            pass

        if isFloat is True:
            return self.__crit_as_float(stmi, rule, rkey)
        else:
            """ die werte werden als strings verglichen,
            lediglich ==/equal wird verglichen
            """
            return self.__crit_as_string(stmi, rule, rkey)

    def __crit_as_float(self, stmi, rule, rkey):
        tru = 0
        for roi in rule[rkey].keys():
            try:
                ri = float(rule[rkey][roi])
                if self.__cr_greater(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__cr_greater_eq(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__cr_lower(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__cr_lower_eq(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__cr_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue

                if self.__cr_not_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue
            except:
                pass
        return tru

    def __crit_as_string(self, stmi, rule, rkey):
        tru = 0
        for roi in rule[rkey].keys():
            try:
                ri = rule[rkey][roi]
                if self.__cr_equal(roi, stmi, ri) == 1:
                    tru += 1
                    continue
            except:
                pass
            return tru

    def __cr_greater(self, roi, stmi, ri):
        try:
            if roi == "greater" and stmi > ri:
                return 1
        except:
            return 0
        return 0

    def __cr_greater_eq(self, roi, stmi, ri):
        try:
            if roi == "greater_equal":
                if stmi > ri or stmi == ri:
                    return 1
        except:
            return 0
        return 0

    def __cr_lower(self, roi, stmi, ri):
        try:
            if roi == "lower" and stmi < ri:
                return 1
        except:
            return 0
        return 0

    def __cr_lower_eq(self, roi, stmi, ri):
        try:
            if roi == "lower_equal":
                if stmi < ri or stmi == ri:
                    return 1
        except:
            return 0
        return 0

    def __cr_equal(self, roi, stmi, ri):
        try:
            if roi == "equal" and stmi == ri:
                return 1
        except:
            return 0
        return 0

    def __cr_not_equal(self, roi, stmi, ri):
        try:
            if roi == "not_equal" and stmi != ri:
                return 1
        except:
            return 0
        return 0
