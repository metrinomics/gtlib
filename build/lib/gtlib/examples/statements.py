"""Test Statements.
        ACHTUNG!!!!!
        Das ist nach dem Survey-Umbau (09.2019), eigentlich falsch, als Input Data
        für Workflows zumindest!!! 
        Mit den Testdaten hier, können Module des Model Classifiers, 
        direkt angesprochen werden!
        Workflows benötigen folgende Struktur:
        [
            {
                "ratings": [
                    { "id": 1, "order": 1 },
                    { "id": 2, "order": 2 }
                ],
                "texts": [ { "id": 1, "text": "asdfasdf" } ],	
                "channel_id": 1,
                "statement_id": 123,
                "respondent_id": 123456,
                "timestamp" : 123456789
            }
        ]
        """


class ExampleStatements:
    def __init__(self):
        """Configuration Object."""
        self.statements = self.__example_statements()

    def __example_statements(self):
        return [
            {"statement_id": 903468765434,
             "respondent_id": 121987650,
             "timestamp": 1537172653,
             "statement": "all OTHER Responsiveness and PERSON people in the Country UK are extremly good and country usa",
             "att_1": "att a", "att_2": "att b", "att_3": "att x", "att_4": "att xy", "Customer": "ogl", "se": 1.0, "ma": 13.6},
            {"statement_id": 903468765435,
             "respondent_id": 121987650,
             "timestamp": 1537172734,
             "statement": "Some OTHER strengths and the ORGANIZATION Gabi in the Country USA are absolutly happy",
             "att_1": "att d", "att_2": "att x", "att_3": "att f", "Customer": "ugl", "se": 0.5, "ma": 73.6},
            {"statement_id": 9046198765436,
             "respondent_id": 21987651,
             "timestamp": 1537172800,
             "statement": "the Country UK and the ORGANIZATION Alfonso with OTHER support and presence are not very sad and country usa",
             "att_1": "att x", "att_2": "att e", "att_3": "att f", "Customer": "agl", "se": -1.0, "ma": 91.6},
            {"statement_id": 9034661765437,
             "respondent_id": 21987651,
             "statement": "in the Country USA OTHER have impression of little prices are very bad and awful",
             "att_1": "att a", "att_2": "att b", "att_3": "att f", "Customer": "ogl", "se": 0.7, "ma": 10.2},
            {"statement_id": 9000000000015,
             "respondent_id": 12345000,
             "timestamp": 1537172900,
             "statement": "I like the personal interaction of the interview. I also like having the information in front of me, which is the first time I've done it this way. Normally country usa this kind of interview is either just an Internet survey or just over the phone. I like having the combination of the two.",
             "att_1": "att a", "att_2": "att e", "att_3": "att c", "Customer": "ugl", "se": -0.7, "ma": 5.2},
            {"statement_id": 9000000000016,
             "respondent_id": 12345000,
             "statement": "Because of the nature of the interview, I'm not sure how Josef in country usa would view us taking part in this kind of place. There are certain components that focus so much on competing with Josef. I don't think that is a very comfortable position for either Josef or Mohamed. I think the interview should be aimed at Sprint staff only.",
             "att_1": "att d", "att_2": "att b", "att_3": "att c", "att_4": "att ef", "Customer": "agl", "se": -0.7, "ma": 58.2},
            {"statement_id": 9000000000017,
             "respondent_id": 12345001,
             "timestamp": 1537173000,
             "statement": "Responsiveness to emergency requests: Mohamed from country usa has made great improvements in this area. When we had the outage in December, it took us some time to try to convince all the right people that we had an issue going on. It took a lot of re-explaining as to why we couldn't fix the problem ourselves. At that time our customers were unable to make business. However, when we had the outage last Thursday, Mohamed responded very quickly.",
             "att_1": "att a", "att_2": "att e", "att_3": "att f", "Customer": "ogl", "se": 0.45, "ma": 33.2},
            {"statement_id": 9000000000018,
             "respondent_id": 12345001,
             "statement": "When a problem is raised, there should be will for problem analysis and final resolution. Instead, we can see just attempts to mask out or workaround the problem and forwarding it to some other team. Henry should take Günther and Helga as an example for good behaviour.",
             "att_1": "att d", "att_2": "att e", "att_3": "att c", "Customer": "agl", "se": -0.7, "ma": 19.2},
            {"statement_id": 9000000000019,
             "respondent_id": 12345002,
             "timestamp": 1537173100,
             "statement": "Neoclassicismo durou por um tempo anormalmente longo e acabou por sufocar a inovação e restringir a criação literária. Foi só em 1836 que o romantismo começou ao influenciar a poesia brasileira em larga escala, principalmente por meio dos esforços do poeta expatriado Gonçalves de Magalhães.[8] Vários jovens poetas, como Casimiro de Abreu, começaram a fazer experiências com o novo estilo logo depois.[9] Este período produziu algumas das primeiras obras-primas da literatura brasileira.",
             "att_1": "att a", "att_2": "att e", "att_3": "att c", "att_4": "att ef", "att_5": "att az", "Customer": "ugl", "se": 0.3, "ma": 78.2},
            {"statement_id": 9000000000020,
             "respondent_id": 12345003,
             "timestamp": 1537173200,
             "statement": "1922年2月11日から17日までの一週間の間、コーヒー・ブームによって首都リオを凌ぐ勢いで繁栄していたサンパウロで「近代芸術週間」が開催された。アニー タ・マルファッチやディ・カヴァルカンチなどブラジル近代主義の先駆けとなった画家をはじめ、マリオ・デ・アンドラーデ（ポルトガル語版、英語版）、オスヴァルド・デ・アンドラーデ（ポルト ガル語版、英語版）、メノッチ・ピッキア、グラサ・アラーニャなどの作家や彫刻家のヴィクトール・ブレシェレ、音楽家のエイトル・ヴィラ・ロボスなど前衛的な芸術家が一堂に会したこの催 しでは、それまでの芸術思潮への猛批判がなされ、以後のブラジルの知的活動のあり方に多大な変革をもたらした[15]。当初ブラジルの近代主義は美学的な観点のみを問題に していたが",
             "att_1": "att a", "att_2": "att e", "att_3": "att c", "Customer": "ogl", "se": -0.3, "ma": 11.2},
            {"statement_id": 9000000000021,
             "respondent_id": 12345004,
             "timestamp": 1537173300,
             "statement": "، كما ظهر الشعراء: خوزى بازيليو دى جاما، وتوماس أنطونيو جونزاجا. وفي القرن 19 سادت الرومانسية، وطبعت الأدب البرازيلي وكذلك الأدب الأوروبي وظهر من الشعراء في ذلك العصر دومبنجوس خوزى جونسالفى، ومن الأدباء ظهر خوزى دى ألانكار. أما أعظم أديب برازيلي فكان يواكيم ماريا مخادو دى أسيس، وظهر من معاصريه الشاعر الباراناسى أولافو دى بيلاك. أما الأدب البرازيلي في القرن 20 فينحصر في    المشكلات الاجتماعية، والسياسية والفلسفية والسيكلوجية، كما يهتم بقضايا التفرقة العنصرية.ومن أشهر الأدباء في هذا القرن: جيلبرتو فريرى، وروى باربوزا، ويورجى أمادو، واريكو فريسيمو، ويورجى دى ليما، ممن جعلوا نظرة الكتاب البرازيلين في ا ",
             "att_1": "att d", "att_2": "att e", "att_3": "att c", "Customer": "agl", "se": 0.655, "ma": 28.2},
            {"statement_id": 9000000000022,
             "respondent_id": 12345001,
             "timestamp": 1537173400,
             "statement": "Представителем республиканского движения в публицистике явился Силвио Ромеро (1851—1914), выступивший с критикой католической церкви и рабовладельческой монархии. К этому движению примыкал также критик Тобиас Баррето.",
             "att_1": "att d", "att_2": "att b", "att_3": "att f", "Customer": "ugl", "se": -0.247, "ma": 86.2},
            {"statement_id": 9000000000023,
             "respondent_id": 12345005,
             "timestamp": 1537173500,
             "statement": "بنتوتیکز با پینتو (۱۶۱۸-۱۵۴۰) اولین شاعری بود که اشعاری جالب سرود و مانوئل بوتلود اولیویرا Oliveria (۱۷۱۱-۱۶۳۶) نیز اشعار جالبی پدید آورد.از ادیبان دیگر سده هفدهم فری ویسنت دوسالوادور (۱۶۳۶-۱۶۶۴) و گریگوریود ماتوس گیورا Guerra (۱۶۹۶-۱۶۶۳) و آنتونیو وییرا (۱۶۹۷-۱۶۰۸) و سرمائوس Sermoes (۱۷۴۸-۱۶۷۹) را می‌توان نام برد که هر یک آثار ادبی جالبی اعم از نظم و نثر خلق کردند. ",
             "att_1": "att d", "att_2": "att e", "att_3": "att c", "Customer": "ogl", "se": 0.257, "ma": 1.2}
        ]
