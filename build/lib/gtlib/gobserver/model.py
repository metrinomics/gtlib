"""ORM Maper Objects für Observer DB.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak

PM/api/projects ->
{
    "models": {
        "1": {
            "18139": {
                "id": 18139,
                "label": "Observer_A",
                "type": "observer",
                "algo": "observer",
                "active": true,
                "build_size": 0,
                "desc": "Erster Observer Test - volumenbasiert",
                "configuration": {
                    "label": "observer_a",
                    "note": [],
                    "output": {},
                    "type": "observer",
                    "input": {  "data_pool": 1000, "size": 100,
                                "time": 0, "type": "workflow" },
                    "model": [  { "project": 1, "workflow": 44 },
                                { "project": 1, "workflow": 50 } ]
                },
                "update": { "data_pool": 0, "size_trigger": 0,
                            "time_trigger": 0 }
            }, ...
    "workflows": {
        "1": {
            "152": [
                {   "action": "classify", "map": {},
                    "model": {  "build": "last", "label": "Observer_A",
                                "type": "observer" } } ],
            "153": [
                {   "action": "classify", "map": {},
                    "model": {  "build": "last", "label": "Observer_A",
                                "type": "observer" } } ],
"""

from sqlalchemy import Column, Index
from sqlalchemy import ForeignKey  # , ForeignKeyConstraint
from sqlalchemy import Integer, DateTime, String  # Float
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class ObserverModel:
    """Defines observer model, also for DB."""

    def __init__(self, con, meta):
        """Init with con and meta of DB."""
        self.__con = con
        self.__meta = meta

    def create_db(self):
        """Create the DB."""
        Base.metadata.create_all(self.__con)

class ObserverConsumer(Base):
    """ObserverConsumer."""

    __tablename__ = "observer_consumer"

    """
    Die Workflows, welche Queues als Abnehmer / Konsumenten aboniert haben,
    sind hier gelistet. Das bedeutet, das diese Workflows das Observer-Modul
    als platziertes Element, besitzen!
        PM/api/projects ->
    """
    id = Column(Integer, primary_key=True)
    # workflows -> x
    project = Column(Integer, nullable=False)
    # workflows[x] -> y
    workflow = Column(Integer, nullable=False)

    Index('idx_project_workflow', project, workflow, unique=True)

class ObserverObserver(Base):
    """ObserverObserver, summerizes ObersverProducer."""

    __tablename__ = "observer_observer"

    id = Column(Integer, primary_key=True)
    label = Column(String, nullable=False, default="observer")
    pmid = Column(Integer, nullable=False)
    project = Column(Integer, nullable=False)

    Index('idx_observer_pmid', label, pmid, unique=True)

class ObserverProducer(Base):
    """ObserverProducer."""

    __tablename__ = "observer_producer"

    """
    Daten-Produzenten sind bisher Workflows, welche ihre Produkte via
    ObserverManager, in eine Queue posten.
        PM/api/projects ->
    """
    id = Column(Integer, primary_key=True)
    # models[x][y].configuration.model.[z].project
    project = Column(Integer, nullable=False)
    # models[x][y].configuration.model.[z].workflow
    workflow = Column(Integer, nullable=False)
    # models[x] -> y
    # observer = Column(Integer, nullable=False)
    observer = Column(Integer, ForeignKey(
        ObserverObserver.id), nullable=False)

    Index('idx_project_workflow_observer',
          project, workflow, observer, unique=True)

class ObserverManager(Base):
    """ObserverManager."""

    __tablename__ = "observer_manager"

    """
    Hier werden die Update Zyklen der Queues verwaltet und Observer
    und Consumer werden zusammengeführt

    type = [ "model", "workflow" ]
    """

    id = Column(Integer, primary_key=True)
    observer = Column(Integer, ForeignKey(
        ObserverObserver.id), nullable=False)
    consumer = Column(Integer, ForeignKey(
        ObserverConsumer.id), nullable=False)

    # models[x][y][configuration][input][type] == workflow
    type = Column(String, nullable=False, default="workflow")

    """ Datenmenge zum rechnen
    models[x][y][configuration][input][data_pool]
    """
    data_pool = Column(Integer, index=True, nullable=False)

    # models[x][y][configuration][input][size]
    trigger_size = Column(Integer, index=True, nullable=False)
    # models[x][y][configuration][input][time]
    trigger_time = Column(Integer, index=True, nullable=False)

    Index('idx_observer_consumer_type',
          observer, consumer, type, unique=True)

class ObserverJobs(Base):
    """ObserverJobs."""

    __tablename__ = "observer_jobs"
    """
    Verwaltugn der Jobs und der stati
    status = [ "enabled", "disabled", "running", "failed" ]
    """
    id = Column(Integer, primary_key=True)
    manager = Column(Integer, ForeignKey(ObserverManager.id), primary_key=True)

    # Start-Zeitpunkt von Update
    time_start = Column(DateTime)
    # End-Zeitpunkt von Update
    time_end = Column(DateTime)

    status = Column(String, nullable=False, default="enabled")
