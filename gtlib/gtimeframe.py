"""Time management tools."""

import logging
import datetime
from dateutil.relativedelta import relativedelta, MO


class GTimeFrame:
    """Calculate a timeframes and -deltas."""

    def __init__(self, my_now=datetime.datetime.utcnow()):
        """Calculate a timeframe."""
        self.my_now = my_now
        self.my_end = 0
        self.my_dif = 0
        self.timeframes = {}

    def convert_datetime_to_unixtimestamp(self, dateTimeIn=datetime.datetime.utcnow()):
        """Convert a datetime to unix timestamp."""
        return dateTimeIn.timestamp()

    def get_new_datetime_now(self):
        return datetime.datetime.utcnow()

    def get_as_unixtimestamp(self):
        """Get now as unix timestamp."""
        return self.my_now.timestamp()

    def set_now_from_unixtimestamp(self, uxt):
        """Set now via unix timestamp."""
        # self.my_now = datetime.datetime.fromtimestamp(uxt)
        try:
            self.my_now = datetime.datetime.utcfromtimestamp(uxt)
        except Exception as e:
            logging.error(e)
            self.my_now = self.update_now()
        return self.my_now

    def set_end(self):
        self.time_difference_from_now_in_seconds()

    def set_now(self, my_new_now=None):
        """Set now via datetime object."""
        if isinstance(my_new_now, datetime.datetime):
            self.my_now = my_new_now
            return True
        else:
            return False

    def update_now(self):
        """Generate new now."""
        self.my_now = datetime.datetime.utcnow()

    def time_difference_from_now_in_seconds(self):
        """Calculate a time difference."""
        self.my_end = datetime.datetime.utcnow()
        self.my_dif = self.my_end - self.my_now

    def calc_tf_year(self, myStart=None, myEnd=1):
        if myStart is None:
            myStart = self.my_now.replace(month=1, day=1, hour=0,
                                          minute=0, second=0,
                                          microsecond=0)
        theE = myStart + relativedelta(years=myEnd)
        self.timeframes["year"] = {"start": myStart, "end": theE}

    def calc_tf_month(self, myStart=None, myEnd=1):
        if myStart is None:
            myStart = self.my_now.replace(day=1, hour=0, minute=0,
                                          second=0, microsecond=0)
        theE = myStart + relativedelta(months=myEnd)
        self.timeframes["month"] = {"start": myStart, "end": theE}

    def calc_tf_week(self, myStart=None, myEnd=7):
        if myStart is None:
            myStart = self.my_now + relativedelta(weekday=MO(-1))
            myStart = myStart.replace(hour=0, minute=0,
                                      second=0, microsecond=0)
        theE = myStart + relativedelta(days=myEnd)
        self.timeframes["week"] = {"start": myStart, "end": theE}

    def calc_tf_day(self, myStart=None, myEnd=1):
        if myStart is None:
            myStart = self.my_now.replace(
                hour=0, minute=0, second=0, microsecond=0)
        theE = myStart + relativedelta(days=myEnd)
        self.timeframes["day"] = {"start": myStart, "end": theE}

    def calc_tf_hour(self, myStart=None, myEnd=60):
        if myStart is None:
            myStart = self.my_now.replace(minute=0, second=0, microsecond=0)
        theE = myStart + relativedelta(minutes=myEnd)
        self.timeframes["hour"] = {"start": myStart, "end": theE}

    def calc_tf_minute(self, myStart=None, myEnd=1):
        if myStart is None:
            myStart = self.my_now.replace(second=0, microsecond=0)
        theE = myStart + relativedelta(minutes=myEnd)
        self.timeframes["minute"] = {"start": myStart, "end": theE}

    def calc_tf_microsecond(self, myStart=None, myEnd=1):
        if myStart is None:
            myStart = self.my_now.replace(microsecond=0)
        theE = myStart + relativedelta(seconds=myEnd)
        self.timeframes["second"] = {"start": myStart, "end": theE}

    def time_seg_hour(self, myStep, segmentation):
        tseg = []
        myStart = self.my_now.replace(minute=0, second=0, microsecond=0)

        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(minutes=(myPkg * 60))),
                "end": (myStart - relativedelta(minutes=((myPkg - segmentation) * 60)))})

        return tseg

    def time_seg_day(self, myStep, segmentation):
        tseg = []
        myStart = self.my_now.replace(
            hour=0, minute=0, second=0, microsecond=0)

        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(days=myPkg)),
                "end": myStart - relativedelta(days=(myPkg - segmentation))})

        return tseg

    def time_seg_microsecond(self, myStep, segmentation):
        tseg = []
        myStart = self.my_now.replace(microsecond=0)

        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(seconds=myPkg)),
                "end": (myStart - relativedelta(seconds=(myPkg - segmentation)))})

        return tseg

    def time_seg_minute(self, myStep, segmentation):
        tseg = []
        myStart = self.my_now.replace(second=0, microsecond=0)

        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(minutes=myPkg)),
                "end": (myStart - relativedelta(minutes=(myPkg - segmentation)))})

        return tseg

    def time_seg_month(self, myStep, segmentation):
        tseg = []
        myStart = self.my_now.replace(
            day=1, hour=0, minute=0, second=0, microsecond=0)

        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(months=myPkg)),
                "end": (myStart - relativedelta(months=(myPkg - segmentation)))})

        return tseg

    def time_seg_week(self, myStep, segmentation):
        tseg = []
        myStart = self.my_now + relativedelta(weekday=MO(-1))
        myStart = myStart.replace(hour=0, minute=0, second=0, microsecond=0)
        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(days=(myPkg * 7))),
                "end": (myStart - relativedelta(days=((myPkg - segmentation) * 7)))})

        return tseg

    def time_seg_year(self, myStep, segmentation):
        tseg = []

        myStart = self.my_now.replace(month=1, day=1, hour=0,
                                      minute=0, second=0,
                                      microsecond=0)

        for ts in range(myStep, 0, -1):
            myPkg = ts * segmentation
            tseg.append({
                "start": (myStart - relativedelta(years=myPkg)),
                "end": (myStart - relativedelta(years=(myPkg - segmentation)))})

        return tseg

    def time_segments(self, time_frame=3, segmentation=2, unit="year"):
        myStep = int(time_frame / segmentation)

        if unit == "year":
            return self.time_seg_year(myStep, segmentation)
        elif unit == "month":
            return self.time_seg_month(myStep, segmentation)
        elif unit == "week":
            return self.time_seg_week(myStep, segmentation)
        elif unit == "day":
            return self.time_seg_day(myStep, segmentation)
        elif unit == "hour":
            return self.time_seg_hour(myStep, segmentation)
        elif unit == "minute":
            return self.time_seg_minute(myStep, segmentation)
        elif unit == "second":
            return self.time_seg_microsecond(myStep, segmentation)

        return []

    def calc_tf(self):
        """Calculate different timeframes/-sets."""
        self.timeframes["overall"] = {"start": 0, "end": 0}
        self.calc_tf_year()
        self.calc_tf_month()
        self.calc_tf_week()
        self.calc_tf_day()
        self.calc_tf_hour()
        self.calc_tf_minute()
        self.calc_tf_microsecond()
