"""PostgresSql API."""

import os
import sqlalchemy


class GPgSql:
    """PostgresSql API."""

    def __init__(self):
        """General-API handling Tool."""
        self.__con = ""
        self.__meta = ""
        self.session = ""

        self.__connect()

    def getConMeta(self):
        """Return connection and meta."""
        return self.__con, self.__meta

    def __connect(self):
        url = "postgresql://{}:{}@{}/{}".format(
            os.environ['POSTGRESUSER'],
            os.environ['POSTGRESPWD'],
            os.environ['POSTGRESSERVER'],
            os.environ['POSTGRESDB'])

        # connection onject
        self.__con = sqlalchemy.create_engine(url, client_encoding="utf8")

        # meta data
        self.__meta = sqlalchemy.MetaData(bind=self.__con, reflect=True)
