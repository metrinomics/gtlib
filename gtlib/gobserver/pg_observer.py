"""PostgresSql API."""

import os
import sqlalchemy
from gtlib.gobserver.model import ObserverProducer, ObserverObserver
from gtlib.gobserver.model import ObserverConsumer, ObserverManager
from gtlib.gobserver.model import ObserverJobs
from sqlalchemy.orm import sessionmaker, scoped_session


class PgObserver:
    """PostgresSql Observer API."""

    def __init__(self):
        """General-API handling Tool."""
        self.__con = ""
        self.__meta = ""
        self.session = ""

        self.producer = ObserverProducer
        self.observer = ObserverObserver
        self.consumer = ObserverConsumer
        self.manager = ObserverManager
        self.jobs = ObserverJobs

        self.__connect()

    def __connect(self):
        url = "postgresql://{}:{}@{}/{}".format(
            os.environ['POSTGRESUSER'],
            os.environ['POSTGRESPWD'],
            os.environ['POSTGRESSERVER'],
            os.environ['POSTGRESDB'])

        # connection onject
        self.__con = sqlalchemy.create_engine(
            url, client_encoding="utf8", echo=False)

        # meta data
        self.__meta = sqlalchemy.MetaData(bind=self.__con, reflect=True)

        # establish a session
        self.session = scoped_session(sessionmaker())
        self.session.configure(
            bind=self.__con, autoflush=False, expire_on_commit=False)
