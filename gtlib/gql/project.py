"""GQL Client lib.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
import logging


class ProjectO(GqlO):
    """GQL Client lib."""

    def __init__(self, configO):
        GqlO.__init__(self, configO)

    def gql_projects(self):
        qu = {'query': 'query{ projects{id}}'}
        return self.check_result("projects", qu)

    def gql_project(self, project_id=0):
        qu = {
            'query': '''query{{
                project(id:{}){{
                    name
                    id
                    company {{id}}
                    projectWorkflows {{
                      active
                      id
                      label
                      type
                      description                      
                      steps {{  
                        action
                        map {{ content }}
                        model {{
                          id
                          label
                          algorithm {{ label, suite }}
                        }}
                        stepOrder
                        projectEnabledAlgorithm {{
                            algorithm {{ id, label, suite }}
                        }}
                      }}
                      surveys {{ 
                          id 
                          label
                          questions {{ id, label }}
                        }}
                        project {{ id }}
                    }}
                    projectEnabledWorkflows {{
                        active
                        workflow {{ type }}
                    }}
                    projectEnabledAlgorithms {{
                        algorithm {{
                            active
                            configuration
                            active
                            description
                            suite
                            label
                            models {{
                                label
                                id
                                active
                                configuration
                                description
                                buildSize
                                updateDataPool
                                updateSizeTrigger
                                timeTrigger
                                project {{ id }}
                                algorithm {{
                                    label
                                    suite
                                }}
                            }}                            
                        }}
                    }}
                }}
            }}'''.format(project_id)}
        nu = self.check_result("project", qu)
        return self.__pmap(nu)

    def __cfg_workflow(self, qu):
        try:
            wfs = qu["projectEnabledWorkflows"][0]
            wft_l = wfs["workflow"]["type"]
            wft_s = wfs["active"]
            return {wft_l: wft_s}
        except:
            return {}

    def __cfg_algo_item(self, suix, itm):
        if itm.get("active") in [None, False]:
            return True

        if itm["active"] is True:
            il = itm["label"]
            if suix.get(il) is None or len(suix[il]) == 0:
                suix[il] = {
                    "active": itm["active"],
                    "configuration": itm["configuration"]
                }

        return True

    def __wf_steps_model_algo_no_model(self, so, aModel):
        # es gibt algos ohne modelle
        if so.get("projectEnabledAlgorithm") in [None, False]:
            return True
        if so["projectEnabledAlgorithm"].get("algorithm") in [None, False]:
            return True

        st = so["projectEnabledAlgorithm"]["algorithm"]
        if st.get("label"):
            aModel["label"] = st["label"]

        if st.get("suite"):
            aModel["type"] = st["suite"]

        return True

    def __wf_steps_model(self, so):
        aModel = {
            "build": "last",
            "label": "",
            "type": ""
        }

        if so.get("model") in [None, False]:
            self.__wf_steps_model_algo_no_model(
                so, aModel)
            return aModel

        st = so["model"]

        if st.get("label"):
            aModel["label"] = st["label"]

        if st.get("modelBuild"):
            aModel["build"] = st["modelBuild"]

        if st.get("algorithm") and st["algorithm"].get("suite"):
            aModel["type"] = st["algorithm"]["suite"]

        return aModel

    def __wf_steps_map(self, so):
        aMap = {}

        if so.get("map") in [None, False]:
            return aMap

        if so["map"].get("content") in [None, False]:
            return aMap

        st = so["map"]["content"]

        for ky in st.keys():
            aMap[ky] = st[ky]

        return aMap

    def __wf_steps(self, su):
        au = []
        sts = sorted(su["steps"], key=lambda x: x["stepOrder"])
        for st in sts:
            st1 = {
                "action": st["action"],
                "map": self.__wf_steps_map(st),
                "model": self.__wf_steps_model(st)
            }
            au.append(st1)
        return au

    def __wf_surveys(self, su):
        au = []

        for si in su["surveys"]:
            sui = {
                "id": si["id"],
                "label": si["label"],
                "questions": []
            }

            for suqu in si["questions"]:
                aqu = {
                    "id": suqu["id"],
                    "label": suqu["label"]
                }
                sui["questions"].append(aqu)

            au.append(sui)
        return au

    def __wf_head(self, su):
        steps = self.__wf_steps(su)
        surveys = self.__wf_surveys(su)
        aWf = {
            "active": su["active"],
            "id": su["id"],
            "label": su["label"],
            "type": su["type"],
            "desc": su["description"],
            "steps": steps,
            "surveys": surveys
        }
        return aWf

    def __wf(self, qu):
        try:
            wfl = []
            for su in qu["projectWorkflows"]:
                if su.get("active") in [None, False]:
                    continue
                if su["project"]["id"] != qu["id"]:
                    continue
                wfl.append(self.__wf_head(su))
            return wfl
        except:
            return []

    def __pmodel(self, qu):
        try:
            mod_id = {}
            for su in qu["projectEnabledAlgorithms"]:
                if su.get("algorithm"):
                    if su["algorithm"].get("active") in [None, False]:
                        continue
                    if su["algorithm"].get("models") is None:
                        continue
                    if len(su["algorithm"]["models"]) == 0:
                        continue

                    self.__pmodel_itm(qu["id"], mod_id, su["algorithm"])

            mod = []
            for ky in mod_id.keys():
                mod.append(mod_id[ky])

            return mod
        except:
            return []

    def __pmodel_itm(self, project_id, mod_id, algo):
        for model in algo["models"]:
            if project_id != model["project"]["id"]:
                continue
            if model.get("active") in [None, False]:
                continue

            if mod_id.get(model["id"]) is None:
                mod_id[model["id"]] = {
                    "active": model["active"],
                    "id": model["id"],
                    "label": model["label"],
                    "desc": model["description"],
                    "type": model["algorithm"]["suite"],
                    "algo": model["algorithm"]["label"],
                    "build_size": model["buildSize"],
                    "configuration": model["configuration"],
                    "update": {
                        "data_pool": model["updateDataPool"],
                        "size_trigger": model["updateSizeTrigger"],
                        "time_trigger": model["timeTrigger"]
                    }}

    def __cfg_algo_head(self, qu, sui, algo):
        project_id = qu["id"]
        l_suit = algo["suite"]
        l_algo = algo["label"]

        if sui.get(l_suit) is None:
            sui[l_suit] = {}

        if sui[l_suit].get(l_algo) is None:
            sui[l_suit][l_algo] = {}

        if algo.get("models") is None or len(algo["models"]) == 0:
            self.__cfg_algo_item(
                sui[l_suit], algo)
        else:
            for model in algo["models"]:
                if project_id == model["project"]["id"]:
                    self.__cfg_algo_item(
                        sui[l_suit][l_algo], model)

        return True

    def __cfg_algo(self, qu):
        try:
            sui = {}
            for su in qu["projectEnabledAlgorithms"]:
                if su.get("algorithm"):
                    if su["algorithm"].get("active") in [None, False]:
                        continue
                    self.__cfg_algo_head(qu, sui, su["algorithm"])
            return sui
        except:
            return {}

    def __pmap(self, qu):
        algo_out = {
            "id": qu["id"],
            "label": qu["name"],
            "company": qu["company"]["id"],
            "config": {
                "algo": {},
                "workflow": {}
            },
            "workflow": [],
            "model": []
        }
        algo_out["config"]["workflow"] = self.__cfg_workflow(qu)
        algo_out["config"]["algo"] = self.__cfg_algo(qu)
        algo_out["model"] = self.__pmodel(qu)
        algo_out["workflow"] = self.__wf(qu)

        return algo_out
