"""GQL Client lib.

realisiert mit dem requests modul
@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
import requests
import logging

class ChartsO(GqlO):
    """GQL Client lib."""

    def __init__(self, configO):
        GqlO.__init__(self, configO)

    def get_ratingsOverTime(self, project_id=0, project_workflow_id=0):
        """Hole ratingsOverTime von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    ratingsOverTime(projectWorkflowId:{}){{
                        date, value
                        }}
                    }}
                }}'''.format(project_id, project_workflow_id)
        }
        return self.check_result("ratingsOverTime", qu)

    def get_expectation_analysis(self, project_id=0, project_workflow_id=0):
        """Hole expectationAnalysis von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    expectationAnalysis(projectWorkflowId:{}){{
                        low {{ betterCount, expectedCount, worseCount }},
                        average {{ betterCount, expectedCount, worseCount }},
                        high {{ betterCount, expectedCount, worseCount }}
                        }}
                    }}
                }}'''.format(project_id, project_workflow_id)
        }

        return self.check_result("expectationAnalysis", qu)

    def gql_streaming_overview(self, project_id=0, project_workflow_id=0):
        """Hole streamingOverview von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    streamingOverview(projectWorkflowId:{}){{
                        averagePositivity,
                        totalCount,
                        triggersCount,
                        topics{{
                            numberOfTopicsAllRatings,
                            numberOfTopics,
                            count,
                            label,
                            positivity }}
                         }}}}}}'''.format(
                        project_id, project_workflow_id)
        }

        return self.check_result("streamingOverview", qu)

    def get_health_score(self, project_id=0, project_workflow_id=0):
        """Hole halthScore von einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    healthScore(project_workflow_id:{}){{
                        badRatingsCount,
                        mediumRatingsCount,
                        goodRatingsCount }}}}}}'''.format(
                        project_id, project_workflow_id)
        }

        return self.check_result("healthScore", qu)

    def get_topic_positivity(self, project_id=0, project_workflow_id=0):
        """Hole Topic Positivity worte zu einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    topicPositivity(project_workflow_id:{}){{
                        label, negativeCount, positiveCount}}}}}}'''.format(
                        project_id, project_workflow_id)
        }

        return self.check_result("topicPositivity", qu)

    def get_top_topics_no_synonyms(self, project_id=0, numberOfTopics=3,
                                   numberOfWords=3):
        """Hole Top X worte zu einem Projekt, exkludiere synonyme."""
        return self.get_top_topics(
            project_id, numberOfTopics, numberOfWords, "false")

    def get_top_topics(self, project_id=0, numberOfTopics=3,
                       numberOfWords=3, synonyms="true"):
        """Hole Top X worte zu einem Projekt."""
        qu = {
            'query': '''query{{
                project(id:{}){{
                    topTopics(numberOfTopics: {}, numberOfWords: {},
                        synonyms: {} ){{
                        mostFrequentWords, position, topic }}}}}}'''.format(
                        project_id, numberOfTopics, numberOfWords, synonyms)
        }

        return self.check_result("topTopics", qu)

    def get_statements(self, project_id=0):
        """Hole alle Statements zu einem projekt."""
        """
        {
          answerFeed(projectId:1, limit:20) {
            answers { id, statements{ id, content } }
            total } }
        """

        qu = {
            'query': '''query{{
                project(id:{}){{
                    answers{{
                        id,
                        answeredAt,
                        rating {{ id, label, position }}
                        statements {{id, content}} }} }} }}'''.format(
                        project_id)
        }

        return self.check_result("answers", qu)
