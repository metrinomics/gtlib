"""GQL Client lib.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
import logging
import json


class MapO(GqlO):
    """GQL Client lib."""

    def __init__(self, configO):
        GqlO.__init__(self, configO)

    def create_map(self, myParams):
        """Erzeuge Map."""
        md = "MapO.create_map"
        myContent = json.dumps(json.dumps(myParams["content"]))
        qu = {
            'query': '''mutation{{
                createMap(
                    params:{{
                        title: "{}",
                        description: "{}",
                        content: {},
                        type: "{}"
                    }}
                    ){{ 
                        content 
                        description 
                        id 
                        type 
                        title }}
                }} '''.format(
                myParams["title"],
                myParams["description"],
                myContent,
                myParams["type"]
            )
        }
        myMap = self.check_result("createMap", qu)

        myResp = {}
        try:
            myResp = {
                "description": myMap["description"],
                "id": myMap["id"],
                "type": myMap["type"],
                "title": myMap["title"],
                "content": myMap["content"]
            }
        except Exception as e:
            logging.error(myMap)
            logging.error("{} : {}".format(md, e))
            myResp = myMap

        return myResp

    def get_map(self, map_id=0):
        """Hole Map."""
        qu = {
            'query': '''query{{
                map(id:{}){{  
                    content
                    description
                    id
                    type
                    title }}
                }}'''.format(map_id)
        }

        myMap = self.check_result("map", qu)
        return myMap["content"]

    def get_maps(self):
        """Hole maps Übersicht."""
        qu = {
            'query': '''query{
                maps{  
                    title 
                    type 
                    id  
                    description }
                }'''
        }

        return self.check_result("maps", qu)
