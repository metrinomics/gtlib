"""GQL Client lib.

realisiert mit dem requests modul
@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

import requests
import logging


class GqlO:
    """GQL Client lib."""

    def __init__(self, configO):
        """Etabliere Verbindung, hole token."""
        self.configO = configO
        self.headers = {}

        self.__do_login()

    def check_result(self, resItm="", qu=""):
        res = self.__post_data(qu)
        stm = ""
        try:
            if res.status_code == 200:
                rej = res.json()
                if resItm == "":
                    try:
                        stm = rej["data"]["project"]
                    except:
                        stm = rej["data"]
                else:
                    try:
                        stm = rej["data"]["project"][resItm]
                    except:
                        stm = rej["data"][resItm]
            else:
                logging.debug(res.status_code)
                logging.debug(res.text)
                logging.debug(qu)
        except Exception as e:
            logging.error("gtlib.gql.gql.check_result()")
            logging.error(qu)
            logging.error(rej)
            logging.error(e)

        return stm

    def __do_login(self):
        # nicht jede API hat einen login!!!!
        md = "GqlO.__do_login"
        qu = {
            'query': '''mutation{{
                login(email: "{}", password:"{}"){{
                    token}}}}'''.format(
                self.configO.email, self.configO.passwd)
        }
        res = requests.post(self.configO.url, json=qu)

        if res.status_code == 200:
            rej = res.json()
            try:
                token = rej["data"]["login"]["token"]
                self.headers['Authorization'] = 'Bearer {}'.format(token)
            except Exception as e:
                logging.error("{} : {}".format(md, e))
        else:
            logging.error("{} : {}".format(md, res))

    def __post_data(self, my_json):
        res = requests.post(
            self.configO.url,
            json=my_json,
            headers=self.headers
        )
        return res
