"""GQL Client lib.

@copyright: 2020 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
"""

from gtlib.gql.gql import GqlO
# import logging


class ResponsesByRespondentO(GqlO):
    """GQL Client Answer."""

    def __init__(self, configO):
        GqlO.__init__(self, configO)

    def gql_responses_by_respondent(self, shortcode="", respondentId=""):
        qu = {
            'query': '''query{{
                listResponsesByRespondent(shortcode: "{}", respondentId: "{}") {{
                    statements {{ 
                        id 
                        content
                    }}
                    answeredAt
                    respondentId
                    answers {{
                        id
                        text 
                        question {{
                            id 
                            surveyItem {{
                                id 
                                }} 
                            }}
                        rating {{
                            id 
                            position
                        }}
                        options {{
                            id
                        }}
                    }}
                    metadata
                }}
            }}'''.format(shortcode, respondentId)}
        nu = self.check_result("listResponsesByRespondent", qu)
        sor = self.__my_sorter(nu)
        return sor

    def __my_sorter(self, nu):
        sor = []
        try:
            tm = {}

            for i in nu:
                # mky = i['answers'][0]['id']
                mky = i['answeredAt']
                tm[mky] = i

            sok = sorted(tm.keys())

            for i in sok:
                sor.append(tm[i])
        except:
            pass

        return sor
