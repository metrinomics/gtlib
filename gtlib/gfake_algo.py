"""Handy Fake Algos."""

import random


class GFakeAlgo:
    """Fake Algos."""

    def __init__(self, config={}):
        """Fake Algo handling."""
        self.__conf = config

    def rnd_1_to_100(self):
        """Eine Zufallszahl zwischen 1 und 100."""
        out = random.randint(1, 100)
        return out
