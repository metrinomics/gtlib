"""Classify words in a given wordarray, with a given bigram model.

Output:
    'statement_id': statement_id,
    'disassembly': [],
        array von arrays, das statement ist in sätze unterteilt,
        jeder satz ist ein array. jeder satz wurde in ein array von
        worten zerlegt. die worte sind stop-wort bereinigt!
    'sentences': [],
        array von dicts. pro satz ein array. jedes wort wurde klassifiziert.
        die klasse/topic ist der schlüssel des dicts. danach kommt als
        unterschlüssel, das wort. jedes wort ist mit frequenz und signifikanz
        versehen.
    'statement': {},
        analog zu sentence, allerdings auf statement level, ohne
        satzunterteilung

    'words_in_sentences': [],
        stopwort bereinigte sätze, zerlegt in worte, gebündelt auf satzebene
    'words_in_statement': {},
        analog zu words_in_sentences, ohne satzunterteilung

    'words_in_sentences_no_synonyms': [],
        analog zu words_in_sentences, allerdings sind die worte synonym
        bereinigt. synonyme sind begriffe welche einer klasse/topic zugeordnet
        sind.

    'words_in_statement_no_synonyms': {},
        analog zu words_in_sentences_no_synonyms, allerdings auf statement
        level, ohne satzunterteilung

    'agg_class': {}
        Klassifikationsergebnisse hochaggregiert zur Klasse/Topics

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@author: Ruwen Poljak
@note:
ein array von arrays mit worten darin, wird klassifiziert.
die klassifikation erfolgt mittels eines bigram-models.
"""


class ClassifierO:
    """Transform Statement in mapped and merged Array of words."""

    def __init__(self, mm):
        """Transform Statement in mapped and merged Array of words."""
        self.__mm = mm

    def __sentence(self, awrd, colS):
        for aCls in self.__mm.cls_mod_re[awrd].keys():
            mySig = self.__mm.cls_mod_re[awrd][aCls]
            if colS.get(aCls) is None:
                colS[aCls] = {awrd: {"freq": 1, "sig": mySig}}
            else:
                if colS[aCls].get(awrd) is None:
                    colS[aCls][awrd] = {"freq": 1, "sig": mySig}
                else:
                    colS[aCls][awrd]["freq"] += 1

    def __count_all_words(self, awrd, aRes):
        try:
            aRes["words_in_statement"][awrd] += 1
        except:
            aRes["words_in_statement"][awrd] = 1

    def __count_words_per_sentence(self, awrd, aRes, countSen):
        try:
            aRes["words_in_sentences"][countSen][awrd] += 1
        except:
            try:
                aRes["words_in_sentences"][countSen][awrd] = 1
            except:
                aRes["words_in_sentences"].append({awrd: 1})

    def __create_no_synonym_count_sentences(self, aRes):
        for itm in aRes['words_in_sentences']:
            # delete topic.synonyms
            nui = {}
            for i2 in itm.keys():
                if i2 not in self.__mm.cls_mod_re:
                    nui[i2] = itm[i2]
            aRes['words_in_sentences_no_synonyms'].append(nui)

    def __create_no_synonym_count_statement(self, aRes):
        for itm in aRes['words_in_statement'].keys():
            # delete topic.synonyms
            if itm not in self.__mm.cls_mod_re:
                aRes['words_in_statement_no_synonyms'][itm] = aRes['words_in_statement'][itm]

    def __create_no_synonym_count(self, aRes):
        self.__create_no_synonym_count_sentences(aRes)
        self.__create_no_synonym_count_statement(aRes)

    def classify(self, statement_id, wrd_dict):
        """Clear punctzuation, split in sentences."""
        aRes = {'statement_id': statement_id,
                'disassembly': wrd_dict,
                'sentences': [],
                'statement': {},
                'words_in_sentences': [],
                'words_in_statement': {},
                'words_in_sentences_no_synonyms': [],
                'words_in_statement_no_synonyms': {}
                }
        colStm = {}
        countSen = 0
        for wd in wrd_dict:
            colSen = {}
            for awrd in wd:
                self.__count_all_words(awrd, aRes)
                self.__count_words_per_sentence(awrd, aRes, countSen)
                if self.__mm.cls_mod_re.get(awrd) is not None:
                    self.__sentence(awrd, colSen)
                    self.__sentence(awrd, colStm)
            aRes["sentences"].append(colSen)
            countSen += 1
        aRes["statement"] = colStm
        self.__aggregate_classes(aRes)
        self.__create_no_synonym_count(aRes)
        return aRes

    def __aggregate_classes(self, aRes):
        aRes["agg_class"] = {}
        for ki in self.__mm.cls_mod:
            aRes["agg_class"][ki['key']] = 0

        for ki in aRes["statement"].keys():
            for ko in aRes["statement"][ki].keys():
                aRes["agg_class"][ki] += aRes["statement"][ki][ko]["freq"]
