"""Transform Statement in mapped and merged Array of words.

@copyright: 2019 Metrinomics GmbH. All rights reserved.
@note:
ein statement wird in sätze zerlegt. die sätze werden in wort-arrays
unterteilt. stopwords werden aus den arrays entfernt. die verbleibenden
worte werden gemappt und gemerged, unter zuhilfenahme der entsprechenden
listen. für doppelte begriffe in einem satz gilt, das sie nur einfach
gezählt werden.

BEISPIEL
  diminish is better diminishes and diminish.

diminish taucht in der mapping-liste auf:
diminish(*) = [diminish, diminishes ]
vor dem mapping wird der satz auf doppelte begriffe untersucht,
diese werden zu einem begriff zusammengefasst:

  diminish is better diminishes and diminish.
wird zu:
  diminish is better diminishes and.

better ist ein mapping von good(*)
"is" und "and" sind Stopwords und werden ignoriert.

  diminish is better diminishes and diminish.
wird zu:
  diminish is better diminishes and.
wird zu:
  diminish better diminishes.
wird zu:
  ['diminish(*)', 'good(*)', 'diminish(*)']

es wurde eine differenzierte zählung von "doppelten", bzw. "ähnlichen"
begriffen, realisiert"
"""

import logging


class StmWordDictO:
    """Transform Statement in mapped and merged Array of words."""

    def __init__(self, mms):
        """Transform Statement in mapped and merged Array of words."""
        self.__mms = mms
        self.__chr_to_del = [".", ",", ";", "#", "„", "“", "*", "/", "\\",
                             "-", "(", ")", "{", "}", "[", "]"]

    def word_dict(self, astm):
        """Clear punctuation, split in sentences."""
        ml = "gbigram.word_dict.word_dict"
        tSenAsWords = []
        bstm = self.dict_punctuation(astm)
        # pro satz splitten
        for sspl in bstm.split("."):
            try:
                senAsWords = []
                for i in self.__chr_to_del:
                    sspl = sspl.replace(i, "")
                # satz splitten in worte
                senAsWords = sspl.split()
                if len(senAsWords) > 0:
                    tSenAsWords.append(self.__dict_loop(senAsWords))

            except Exception as e:
                logging.debug("{}: {}".format(ml, e))
        return tSenAsWords

    def __dict_loop(self, senAsWords):
        ml = "gbigram.stm_word_dict.__dict_loop"
        try:
            # print(senAsWords)
            cl1 = self.__dict_map(senAsWords)
            # print(cl1)
            cl2 = self.__dict_merge(cl1)
            # print(cl2)
            cl3 = self.__dict_stop(cl2)
            # print(cl3)
            cl4 = self.__dict_map(cl3)
            # print(cl4)
            clFin = self.__dict_merge(cl4)
            # print(clFin)
        except Exception as e:
            logging.debug("{}: {}".format(ml, e))

        """
        print("-"*30)
        for sw in self.__mms.wstop:
            print("\t{}".format(sw))
        print("-"*30)
        """

        return clFin

    def __dict_map(self, senAsWords):
        if len(self.__mms.wmap_re) == 0:
            return senAsWords

        cleared = []
        for wrd in senAsWords:
            try:
                if self.__mms.wmap_re[wrd]:
                    cleared.append(self.__mms.wmap_re[wrd])
                else:
                    cleared.append(wrd)
            except:
                cleared.append(wrd)
        return cleared

    def __dict_stop(self, senAsWords):
        if len(self.__mms.wstop) == 0:
            return senAsWords

        wrdCnt = {}
        cleared = []
        for wrd in senAsWords:
            if wrd not in self.__mms.wstop:
                try:
                    if wrdCnt[wrd] > 0:
                        pass
                except:
                    wrdCnt[wrd] = 1
                    cleared.append(wrd)
        return cleared

    def __dict_merge(self, senAsWords):
        if len(self.__mms.wmerge_re) == 0:
            return senAsWords

        # ml = "classifier.bigram.word_dict.__dict_merge"
        maxPos = len(senAsWords) - 1
        for wp in range(0, maxPos):
            w1 = senAsWords[wp]
            w2 = senAsWords[(wp + 1)]

            try:
                if self.__mms.wmerge_re[w1][w2]:
                    senAsWords[wp] = ""
                    senAsWords[(wp + 1)] = self.__mms.wmerge_re[w1][w2]
            except Exception as e:
                # logging.debug("{}: {}".format(ml, e))
                pass

        cleared = []
        for wp in senAsWords:
            if wp is not "":
                cleared.append(wp)

        return cleared

    def dict_punctuation(self, astm):
        """Clear punctuation in statement."""
        ml = "gbigram.stm_word_dict.__word_dict_punctuation"
        # text bereinigen
        try:
            astm = astm.lower()
            astm = astm.replace("etc.", "etc ")
            astm = astm.translate(str.maketrans("?!", ".."))
            for i in ["'", "`", "'", '"']:
                astm = astm.replace(i, "")
            for i in [":"]:
                astm = astm.replace(i, " ")
        except Exception as e:
            logging.debug("{}: {}".format(ml, e))

        return astm
