"""Configuration Object."""

import os


class GConfigO:
    """Configuration Object."""

    def __init__(self, style=""):
        """Configuration Object."""
        self.header = {"Content-Type": "application/json"}

        self.protocol = "http"
        self.host = ""

        self.email = ""
        self.passwd = ""
        self.url = ""

        self.testprojectid = 1
        self.testmapid = 7
        self.testworkflowid = 40

        self.flask_env = ""
        self.flask_app = ""

        self.POSTGRESSERVER = "127.0.0.1:5432"
        self.POSTGRESDB = ""
        self.POSTGRESUSER = ""
        self.POSTGRESPWD = ""

        self.tf_workflows = None
        self.tf_project_rest_api = None
        self.tf_model_generator_rest_api = None

        # define config-object
        self.__straight_paths()
        self.__set_gql()
        self.__set_test()

        if style in ["observer", "workflows", "classifier", "generator"]:
            self.__set_defaults()
            self.__set_flask()

        if style in ["observer", "generator"]:
            self.__set_postgress()

        if style == "workflows":
            self.__set_classifier()

    def __straight_paths(self):
        """ das soll sukzessive das neue format werden.
        statt urls via APISERVER und APIPROTOKOL zu bauen,
        soll das zusammenbauen prio 2 erhalten. 
        die links sollen direkt defineirt werden und direkt
        angesprochen/übergeben werden.
        sie bsp.: workflows/tests/test_wf01.py, dort
        zeile 34:
        self.__configO.tf_workflows
        wird direkt an die API übergeben        
        """

        try:
            self.tf_workflows = "{}".format(os.environ['TF_WORKFLOWS'])
        except:
            pass

        try:
            self.tf_project_rest_api = "{}".format(
                os.environ['TF_PROJECT_REST_API'])
        except:
            pass

        try:
            self.tf_model_generator_rest_api = "{}".format(
                os.environ['TF_MODEL_GENERATOR_REST_API'])
        except:
            pass

    def __set_classifier(self):
        try:
            self.host = "{}".format(os.environ['CLASSIFIER_SERVER'])
        except:
            pass
        try:
            self.protocol = os.environ['CLASSIFIER_PROTOKOL']
        except:
            pass

    def __set_flask(self):
        try:
            self.flask_env = os.environ['FLASK_ENV']
        except:
            pass

        try:
            self.flask_app = os.environ['FLASK_APP']
        except:
            pass

    def __set_defaults(self):
        try:
            self.host = "{}".format(os.environ['APISERVER'])
        except:
            try:
                self.host = "{}".format(os.environ['CLASSIFIER_SERVER'])
            except:
                pass

        try:
            self.protocol = os.environ['APIPROTOKOL']
        except:
            try:
                self.protocol = os.environ['CLASSIFIER_PROTOKOL']
            except:
                pass

    def __set_gql(self):
        try:
            self.email = os.environ['GQL_EMAIL']
        except:
            pass

        try:
            self.passwd = os.environ['GQL_PASSWD']
        except:
            pass

        try:
            self.url = "{}".format(os.environ['GQL_URL'])
        except:
            pass

    def __set_test(self):
        try:
            self.testprojectid = os.environ['TESTPROJECTID']
        except:
            try:
                self.testprojectid = os.environ['TEST_PROJECT_ID']
            except:
                pass

        try:
            self.testmapid = os.environ['TESTMAPID']
        except:
            try:
                self.testmapid = os.environ['TEST_MAP_ID']
            except:
                pass

        try:
            self.testworkflowid = os.environ['TESTWORKFLOWID']
        except:
            try:
                self.testworkflowid = os.environ['TEST_WORKFLOW_ID']
            except:
                pass

    def __set_postgress(self):
        try:
            self.POSTGRESSERVER = os.environ['POSTGRESSERVER']
        except:
            pass
        try:
            self.POSTGRESDB = os.environ['POSTGRESDB']
        except:
            pass
        try:
            self.POSTGRESUSER = os.environ['POSTGRESUSER']
        except:
            pass
        try:
            self.POSTGRESPWD = os.environ['POSTGRESPWD']
        except:
            pass
