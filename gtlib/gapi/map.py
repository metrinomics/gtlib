"""General APITools Map.

Es handelt sich um eine GraphQl abfrage, welche hier lediglich umgemappt
wird. Eine REST-Abfrage findet nicht statt!
"""

import logging
from gtlib.gql.map import MapO


class MapApi():
    """General-API handling Tool."""

    def __init__(self, configO):
        self.__configO = configO
        self.__maO = MapO(self.__configO)

    def read_map(self, myMap):
        """Read a map."""
        ml = "gtlib.gapi.map.read_map"
        inData = {}
        try:
            inData = self.__maO.get_map(myMap)
        except Exception as e:
            logging.error("{} : {}".format(ml, e))
        return inData

    def read_maps(self):
        """Read a map-list."""
        ml = "gtlib.gapi.map.read_maps"
        inData = []
        try:
            inData = self.__maO.get_maps()
        except Exception as e:
            logging.error("{} : {}".format(ml, e))
        return inData
