"""General APITools.

Hier werden REST-APIs angesprochen. 
"""

from gtlib.gtool import GTool
import requests
import logging
import re


class GGetInsecure:
    """Get from url without certificate."""

    def __init__(self):
        """Get insecure, no certificate."""
        self.__gtool = GTool()

    def insecure_get_data_no_validation(self, finUrl=""):
        """insecure_get_data_no_validation data and check it.
        no validation, open for
        man in the middle attacks!!!!!!!!!!!
        https://stackoverflow.com/questions/28667684/python-requests-getting-sslerror
        """
        ml = "GGet.insecure_get_data_no_validation"
        try:
            rp = requests.get(finUrl, verify=False)
            return self.__gtool.check_status_code(rp)
        except Exception as e:
            logging.error("{}: {}".format(ml, e))
            return []
