"""General APITools.

Hier werden REST-APIs angesprochen. 
"""

from gtlib.gtool import GTool
import requests
import logging


class GApi:
    """General-API handling Tool."""

    def __init__(self, configO):
        """General-API handling Tool."""
        self.configO = configO
        self.__gtool = GTool()

    def check_get_data_as_get(self, full_url="", myHeader=""):
        ml = "GApi.check_get_data_as_get"
        finUrl = self.pretty_uri(full_url=full_url)

        try:
            rp = requests.get(
                finUrl,
                headers=myHeader)
            return self.__gtool.check_status_code(rp)
        except Exception as e:
            logging.error("{}: {}".format(ml, e))
            return []

    def check_get_data(self, a_uri="", jsBody={}, full_url=""):
        """Get data as POST and check it."""
        ml = "GApi.check_get_data"
        finUrl = self.pretty_uri(a_uri, full_url)

        try:
            rp = requests.post(
                finUrl,
                json=jsBody,
                headers=self.configO.header)
            return self.__gtool.check_status_code(rp)
        except Exception as e:
            logging.error("{}: {}".format(ml, e))
            return []

    def pretty_uri(self, a_uri="", full_url=""):
        """Build a pretty uri."""
        myUri = None
        # prüfe ob eine vollständioge URL (full_url) geliefert wurde
        if len(full_url) > 6 and (full_url[0:7] == "http://" or full_url[0:8] == "https://"):
            # vollständige url wurde geliefert, nutze sie
            myUri = full_url
        else:
            # keine vollständige url wurde geliefert, versuche eine zu bauen
            try:
                myUri = "{}://{}".format(
                    self.configO.protocol,
                    self.configO.host)
            except Exception as e:
                logging.debug("GApi.pretty_uri: {}".format(e))
                return None

        # setze anhang an url
        if a_uri != "":
            myUri = "{}/{}".format(myUri, a_uri)

        return myUri
