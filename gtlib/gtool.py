"""General Tools."""

import logging


class GTool:
    """General Tools."""

    def __init__(self):
        """General Tools."""
        pass

    def get_model_def(self, project, req):
        """Get Model Definition."""
        mpm = {}
        try:
            for pm in project['model']:
                if req['model']['type'] == pm['type']:
                    if req['model']['label'] == pm['label']:
                        mpm = pm
                        break
        except Exception as e:
            ml = "gtlib.GTool.get_model_def"
            logging.debug("{}: {}".format(ml, e))
        return mpm

    def metrinomics_build_path_get_model(self, project, type, label):
        """Hole das Model aus der Projekt Definition."""
        metrij = {}
        metrij = self.__get_conf_unknown_suite(project, type, label)
        if metrij is None or len(metrij) == 0:
            metrij = self.__get_conf_unknown_algo(project, type, label)
        return metrij

    def __get_conf_unknown_suite(self, project, type, label):
        metrij = {}
        pca = project["config"]["algo"]
        """
            die suite(framework) ist unbekannt. suche alles ab nach
            algo(type) und label(model)
        """
        for suite in pca.keys():
            if pca[suite].get(type):
                if pca[suite][type].get(label):
                    metrij = pca[suite][type][label]["configuration"]
                    break
        return metrij

    def __get_conf_unknown_algo(self, project, suite, label):
        metrij = {}
        pca = project["config"]["algo"]
        """
            es kann sein, das nur suite(framework) und label(model) bekannt
            sind. es wird versucht einen algo(type) zu finden
        """
        for aType in pca[suite].keys():
            if pca[suite][aType].get(label):
                metrij = pca[suite][aType][label]["configuration"]
                break
        return metrij

    def check_test_defaults(self, pbody, res):
        """Test 2 Structures."""
        if len(pbody) == 0:
            return {"flag": False,
                    "msg": "FAILURE: found no data in Post-Body"}

        if len(res) == 0:
            return {"flag": False,
                    "msg": "FAILURE: found no data in Response"}

        if len(pbody) != len(res):
            return {"flag": False,
                    "msg": "FAILURE: wrong number of data was classified"}

        return {"flag": True, "msg": "OK"}

    def model_rights(self, mType, mLabel, confAlgo):
        """Prüfen ob der algo verwendet werden darf.
            geliert werden die suite/type und das modell.
            daraus wird der algo abgeleitet. ein algo kommt 
            aus einer suite (ist dort angesiedelt) und wird
            in einem modell verwendet.

            type == suite
            label == model label
            right => True / False == use / dont use            
        """
        ml = "gtlib.GTool.model_rights"
        mRights = {
            "right": False,
            "type": mType,
            "label": mLabel,
            "algo": ""
        }
        if confAlgo.get(mType):
            for ky in confAlgo[mType].keys():
                try:
                    if confAlgo[mType][ky].get(mLabel):
                        mRights["right"] = confAlgo[mType][ky][mLabel]
                        mRights["algo"] = ky
                except Exception as e:
                    logging.debug("DISABLED ALGO {}: {}".format(ml, e))
                    pass
        return mRights

    def check_status_code(self, rp):
        """Check status code of retrieved data."""
        ml = "gtlib.GTool.check_status_code"
        inData = []
        if rp.status_code in [200, 201]:
            try:
                inData = rp.json()
            except Exception as e:
                logging.error("{}: {}".format(ml, e))
        return inData
